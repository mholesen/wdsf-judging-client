﻿using System;
using System.IO;
using Judging_Client.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class ClientTests
    {
        private DataModel _model;

        [TestInitialize]
        public void FillData()
        {
            _model = new DataModel();
            _model.LoadData(new StreamReader(@"C:\Temp\eJudge\Testen\dance.txt"));
        }

        [TestMethod]
        public void LoadData()
        {
            // Prüfen, dass wir 10 Heats haben:
            Assert.IsTrue(_model.Heats.Count == 20);
        }
    }
}
