﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GrandSlamTransmitter.Model
{
    public static class Model
    {
        public static string[] TPSSplitter(string data)
        {
            List<string> tokens = new List<string>();
            bool upmode = false;
            string token = "";

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == ';' && !upmode)
                {
                    tokens.Add(token);
                    token = "";
                    continue;
                }

                if (data[i] == '"')
                {
                    upmode = !upmode;
                    continue;
                }

                token += data[i];
            }
            // Add the last one at end of line
            tokens.Add(token);

            return tokens.ToArray();

        }
    }
}
