﻿using System.Collections.Generic;

namespace GrandSlamTransmitter.Model
{
    public class Couple
    {
        public int Id { get; set; }

        public string MinID { get; set; }

        public string FirstNameHe { get; set; }
        public string LastNameHe { get; set; }
        public string MINMan { get; set; }

        public string FirstNameShe { get; set; }
        public string LastNameShe { get; set; }
        public string MINWoman { get; set; }
        public string Country { get; set; }

        

        public Dictionary<string, List<Mark>> Results { get; set; }

        public List<Competition> Competitions { get; set; }

        public string WDSFID { get; set; }

        public string NiceName
        {
            get { return this.FirstNameHe + " " + this.LastNameHe + " - " + this.FirstNameShe + " " + this.LastNameShe; }
        }

       
        public Couple()
        {
            this.Competitions = new List<Competition>();
        }

        public Couple Clone()
        {
            var c = new Couple();
            c.FirstNameHe = this.FirstNameHe;
            c.LastNameHe = this.LastNameHe;
            c.FirstNameShe = this.FirstNameShe;
            c.LastNameShe = this.LastNameShe;
            c.Country = this.Country;
            c.Id = this.Id;

            return c;
        }
    }
}