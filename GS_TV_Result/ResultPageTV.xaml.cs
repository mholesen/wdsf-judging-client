﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml;

namespace TVGraphics
{
    /// <summary>
    /// Interaction logic for ResultPage.xaml
    /// </summary>
    public partial class ResultPageTV : Page
    {
        private List<Models.ListResultElement> _model;

        private static Action EmptyDelegate = delegate() { };

        public static void Refresh(UIElement uiElement)
        {
            uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }

        public ResultPageTV(string title, List<Models.ListResultElement> model)
        {
            InitializeComponent();
            this.Title.Content = title;
            _model = model;
        }

        public void ShowResult()
        {
            Video.Play();
            Refresh(this);

            for (int i = _model.Count - 1; i >= 0; i--)
            {
                CreateLine(i, _model[i]);
                this.UpdateLayout();
                Refresh(this);
//                System.Threading.Thread.Sleep(1000);
            }
        }

        public static UIElement cloneElement(UIElement orig)
        {

            if (orig == null)

                return (null);

            string s = XamlWriter.Save(orig);

            StringReader stringReader = new StringReader(s);

            XmlReader xmlReader = XmlTextReader.Create(stringReader, new XmlReaderSettings());

            return (UIElement)XamlReader.Load(xmlReader);

        }

        private void CreateLine(int index, Models.ListResultElement element)
        {
            double top = 384 + (index * 84);

            var place = (Label) cloneElement(this.TempPlace);
            place.Visibility = System.Windows.Visibility.Visible;
            MainCanvas.Children.Add(place);
            Canvas.SetTop(place, top);
            place.Content = element.Place + ".";
            // 
            var points = (Label)cloneElement(this.TempSum);
            points.Visibility = System.Windows.Visibility.Visible;
            MainCanvas.Children.Add(points);
            Canvas.SetTop(points, top);
            points.Content = String.Format("{0:#0.00}", element.Sum);



            var couple = (Label)cloneElement(this.TempCouple);
            couple.Visibility = System.Windows.Visibility.Visible;
            MainCanvas.Children.Add(couple);
            Canvas.SetTop(couple, top);
            couple.Content = element.NameMan + " - " + element.NameWoman;

            if (element.Sum == 0)
            {
                points.Content = "";
                Canvas.SetLeft(couple, 430);
            }

            var country = (Label)cloneElement(this.TempCountry);
            country.Visibility = System.Windows.Visibility.Visible;
            MainCanvas.Children.Add(country);
            Canvas.SetTop(country, top);
            country.Content = element.Country;
        }
    }
}
