﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using CommonLibrary;

using Judging_Client.ModelV1;
using Judging_Client;

namespace Judging_ClientV1
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindowV1 : Window
	{
		private DataModel context = new DataModel();
		private System.Timers.Timer _timer;
		private TimeSpan _timeLeft;
        private Judging_Client.MainWindow _parentWindow;
        private System.Threading.Thread pingThread;
		private Key lastKey = Key.F6;
		
		public MainWindowV1(Judging_Client.MainWindow parentWindow)
		{
			try
			{
                _parentWindow = parentWindow;

				InitializeComponent();

				AppDomain.CurrentDomain.UnhandledException +=
					new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

				this.DataContext = context;
				context.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(Context_PropertyChanged);
                context.CurrentJudgement.PropertyChanged +=
                    new System.ComponentModel.PropertyChangedEventHandler(CurrentJudgement_PropertyChanged);


/*				buttonControl1.Area = Settings.Default.ShortA;
				buttonControl2.Area = Settings.Default.ShortB;
				buttonControl3.Area = Settings.Default.ShortC;
				buttonControl4.Area = Settings.Default.ShortD;
				buttonControl5.Area = Settings.Default.ShortE;

				buttonControl1.Visibility = Settings.Default.ShowA ? Visibility.Visible : Visibility.Hidden;
				buttonControl2.Visibility = Settings.Default.ShowB ? Visibility.Visible : Visibility.Hidden;
				buttonControl3.Visibility = Settings.Default.ShowC ? Visibility.Visible : Visibility.Hidden;
				buttonControl4.Visibility = Settings.Default.ShowD ? Visibility.Visible : Visibility.Hidden;
				buttonControl5.Visibility = Settings.Default.ShowE ? Visibility.Visible : Visibility.Hidden;
                */
				

				HandleNewData();
				ReadJudgingData();

                // Quick Fix für Salsa Show :
                if (context.Judgements[context.CurrentIndex].DanceShortName == "SA" ||
                    context.Judgements[context.CurrentIndex].DanceShortName == "BA" ||
                    context.Judgements[context.CurrentIndex].DanceShortName == "ME")
                {
                    context.ComponentAVisibility = Visibility.Visible;
                    context.ComponentBVisibility = Visibility.Visible;
                    context.ComponentCVisibility = Visibility.Visible;
                    context.ComponentDVisibility = Visibility.Hidden;
                    context.ComponentEVisibility = Visibility.Hidden;
                }

                if (context.Judgements[context.CurrentIndex].DanceShortName == "SSH")
                {
                    context.ComponentAVisibility = Visibility.Hidden;
                    context.ComponentBVisibility = Visibility.Hidden;
                    context.ComponentCVisibility = Visibility.Hidden;
                    context.ComponentDVisibility = Visibility.Visible;
                    context.ComponentEVisibility = Visibility.Hidden;
                }

                this.DataContext = context;

                pingThread = new System.Threading.Thread(SendPing);
                pingThread.IsBackground = true;
                pingThread.Start();

			}catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
				MessageBox.Show(ex.StackTrace);
				throw ex;
			}
		}

		private void ReadJudgingData()
		{
			try
			{
				if(!System.IO.File.Exists(Settings.Default.LocalOutDirectory + "\\Result_" + this.context.Sign + ".txt"))
					return;

				var sr = new System.IO.StreamReader(Settings.Default.LocalOutDirectory + "\\Result_" +
													this.context.Sign + ".txt");
				while (!sr.EndOfStream)
				{
					var data = sr.ReadLine().Split(';');

					int couple = Int32.Parse(data[2]);
					var j = context.Judgements.FirstOrDefault(jd => jd.Couple == data[2] && jd.DanceShortName == data[1]);
					if (j == null)
					{
						throw new Exception("Couple with no. " + couple + " not found!");
					}

					j.MarkA = data[3].ParseString();
					j.MarkB = data[4].ParseString();
					j.MarkC = data[5].ParseString();
					j.MarkD = data[6].ParseString();
					j.MarkE = data[7].ParseString();
				}
				// Wir possitionieren nun den Current Index auf das erste Paar
				while (context.CurrentIndex < context.Judgements.Count && context.CurrentJudgement.MarkB != 0)
				{
					context.CurrentIndex++;
					if (context.CurrentIndex == context.Judgements.Count)
					{
						context.CurrentIndex--;
						break;
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			if(e.ExceptionObject is Exception)
			{
				Exception ex = (Exception) e.ExceptionObject;
				MessageBox.Show(ex.Message);
				MessageBox.Show(ex.StackTrace);
			}
		}


		private void WriteState()
		{
			var task = new Task(() =>
				{

										 int count = 0;
										 System.IO.StreamWriter wr = null;
										 while (wr == null)
										 {
											 try
											 {
												 wr =
													 new System.IO.StreamWriter(Settings.Default.OutDirectory + "\\" + context.DataPath +
																				"\\State" + context.Judge +
																				".txt");
											 }
											 catch (Exception ex)
											 {
                                                 System.Diagnostics.Debug.WriteLine(ex);
												 count++;
												 if (count > 10)
													 return;
												 System.Threading.Thread.Sleep(250);
											 }
										 }

                                         wr.WriteLine(String.Format("{0};{1};{2};{3}",
                                                                    context.Sign,
                                                                    context.IsFinished ? context.CurrentIndex + 1 : context.CurrentIndex,
                                                                    context.CurrentJudgement.DanceNo,
                                                                    0
                                                          ));
										 wr.Close();

									 }

				);
			task.Start();
		}

		void Context_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			WriteState();
		}

		void CurrentJudgement_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			WriteState();

            var control = sender as ButtonControl;
            if (control == null) return;

            Helpbox.Text = context.HelpStrings[control.Area];
			
		}

        private System.IO.StreamReader OpenStream(string filename)
        {
            if (!System.IO.File.Exists(filename)) return null;

            int count = 0;
            System.IO.StreamReader sr = null;
            while (sr == null && count < 30)
            {
                try
                {
                    count++;
                    sr = new System.IO.StreamReader(filename);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    System.Threading.Thread.Sleep(250);
                }
            }

            return sr;
        }

		void HandleNewData()
		{
			try
			{
				int count = 0;
				System.IO.StreamReader sr = OpenStream(Settings.Default.DataDirectory + "\\dance.txt");

                if (sr == null)
                    return;

                // first line is version -> should be 'V1' as this has been
                // checked by our other main window
                sr.ReadLine();
				// Now all other data continues
				context.LoadData(sr);
				sr.Close();
				context.CurrentIndex = 0;
				this.Dispatcher.BeginInvoke(
					(Action)(() => { LockUIFrame.Visibility = System.Windows.Visibility.Collapsed; }));
			}
			catch (Exception ex)
			{
                System.Diagnostics.Debug.WriteLine(ex.Message);
			}
		}

		public void HandleNewState()
		{
#if DEBUG_LOCAL
            var sr = OpenStream(@"c:\eJudge\TestingLocal\newState.txt");
#else
            var sr = OpenStream(Settings.Default.DataDirectory + "\\newState.txt");
#endif
            if (sr == null) return;

			string[] data = sr.ReadLine().Split(';');
			sr.Close();
			// nun müssen wir den Status neu setzen ...
			// heat-Id
            var heatId = 0;
            if(Int32.TryParse(data[0], out heatId))
            {
                context.CurrentIndex = heatId;
            }
		}

		public void HandleNewTime()
		{
            var sr = OpenStream(Settings.Default.DataDirectory + "\\time.txt");
            
            if(sr == null) return;
			// First line is timer 
			TimeSpan time = TimeSpan.Parse(sr.ReadLine());
			sr.Close();           
			// Nun müssen wir die Uhr starten
			this.Dispatcher.BeginInvoke(
					(Action)(() => { TimeLeft.Content = _timeLeft.ToString(); }));

			if (_timer != null)
			{
				_timer.Enabled = false;
			}
			_timer = new Timer(){ AutoReset = false, Enabled = false, Interval = 1000};
			_timer.Elapsed += new ElapsedEventHandler(Timer_Elapsed);     
			_timeLeft = time;
			_timer.Enabled = true;
		}

		void Timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			_timeLeft = _timeLeft.Subtract(new TimeSpan(0, 0, 0, 1));
			this.Dispatcher.BeginInvoke(
				   (Action)(() =>
								{
									TimeLeft.Content = _timeLeft.ToString();
									if (_timeLeft.TotalSeconds < 15)
										TimeLeft.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
									else
										TimeLeft.Foreground = new SolidColorBrush(Color.FromArgb(255, 208, 208, 208));
								}));
			if(_timeLeft.TotalSeconds > 0)
				_timer.Enabled = true;
		}

		void DataChanged(object sender, System.IO.FileSystemEventArgs e)
		{
			// Wir haben eine neue Datendatei, die wir nun einladen wollen
			if (e.Name == "dance.txt")
			{
				HandleNewData();
			}

			if (e.Name == "time.txt")
			{
				HandleNewTime();
			}

			if (e.Name == "newState.txt")
			{
				HandleNewState();
			}

		}

		void ConfirmCanExecute(object sender, CanExecuteRoutedEventArgs e)
		{

            if ((context.CurrentJudgement.MarkA > 0 || buttonControl1.Visibility == Visibility.Hidden) &&
                (context.CurrentJudgement.MarkB > 0 || buttonControl2.Visibility == Visibility.Hidden) &&
                (context.CurrentJudgement.MarkC > 0 || buttonControl3.Visibility == Visibility.Hidden) &&
                (context.CurrentJudgement.MarkD > 0 || buttonControl4.Visibility == Visibility.Hidden) &&
                (context.CurrentJudgement.MarkE > 0 || buttonControl5.Visibility == Visibility.Hidden))
			{
				e.CanExecute = true;
			}
			else
			{
				e.CanExecute = false;
			}
		}

		private void Confirm_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			// Lokal speichern:
			try
			{
				var localwriter = new System.IO.StreamWriter(Settings.Default.LocalOutDirectory + "\\Result_" +
															 this.context.Sign + ".txt");
				context.WriteResult(localwriter);
				localwriter.Close();
			}
			catch (Exception ex)
			{
				// Loggen?
			}

            buttonControl1.ResetBackground();
            buttonControl2.ResetBackground();
            buttonControl3.ResetBackground();
            buttonControl4.ResetBackground();
            buttonControl5.ResetBackground();

			// Asynchron schreiben ...
			Task task = new Task(() =>
									 {
										 try
										 {

											 var writer = new System.IO.StreamWriter(Settings.Default.OutDirectory + "\\" + context.DataPath + "\\Result_" +
																			this.context.Sign + ".txt");
											 context.WriteResult(writer);
											 writer.Close();
										 }
										 catch (Exception ex)
										 {
											 Console.WriteLine(ex.Message);
										 }
									 });

			task.Start();

			if (context.CurrentIndex < context.Judgements.Count - 1)
			{
				context.CurrentIndex++;
                // Quick Fix für Salsa Show :
			}
			else
			{
				this.Dispatcher.BeginInvoke(
					(Action)
					(() => { 
						// We are done: lock the UI
                        context.IsFinished = true;
                        WriteState();
						LockUIFrame.Visibility = System.Windows.Visibility.Visible;
						BtnConfirm.IsEnabled = false;
						Dance.Content = "";
						Couple.Content = "";
                        pingThread.Abort();
                        this.Close();
                        _parentWindow.RestoreWindow();
					}));
			}
		}

		private void BtnHelp_A_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            var area = "";
            if (sender == BtnHelp_A) area = buttonControl1.Area;
            if (sender == BtnHelp_B) area = buttonControl2.Area;
            if (sender == BtnHelp_C) area = buttonControl3.Area;
            if (sender == BtnHelp_D) area = buttonControl4.Area;
            if (sender == BtnHelp_E) area = buttonControl5.Area;

            Helpbox.Text = context.HelpStrings[area];
		}

	    public void CloseWindow()
	    {
	        Dispatcher.Invoke(new Action(Close));
	    }

		private void Window_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
		{

		}

		private void Window_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
		{
            if (lastKey == Key.Q && e.Key == Key.P)
            {
                this.Close();
                _parentWindow.RestoreWindow();
            }
            else
            {
                lastKey = e.Key;
            }
		}

        private void SendPing()
        {
            while (true)
            {
                try
                {
                    WriteState();
                    System.Threading.Thread.Sleep(20000); // sleep 20 sec
                }
                catch(System.Threading.ThreadAbortException)
                {
                    return;
                }
                catch (Exception)
                {
                    System.Threading.Thread.Sleep(20000);
                }
            }
        }

	}
}
