﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Judging_Client.Model.Marking
{
    class DataModel : System.ComponentModel.INotifyPropertyChanged
    {
        public string DataPath { get; set; }

        private string _judge;
        public string Judge
        {
            get { return _judge; }
            set
            {
                _judge = value;
                RaiseEvent("Judge");
            }
        }

        public string Sign { get; set; }
        public string Country { get; set; }

        private string _event;
        public string Event
        {
            get { return _event; }
            set
            {
                _event = value;
                RaiseEvent("Event");
            }
        }

        // private Heat _currentHeat;
        public Heat CurrentHeat
        {
            get { return Heats.Count > 0 ? Heats[CurrentIndex] : null; }
        }

        private int _currentIndex;

        public int CurrentIndex
        {
            get { return _currentIndex; }
            set
            {
                if (value >= Heats.Count)
                {
                    // Do not set CurrentJudgements -> Exception
                    return;
                }
                _currentIndex = value;
                RaiseEvent("CurrentIndex");
                RaiseEvent("CurrentHeat");
            }
        }

        public List<Heat> Heats { get; set; }

        private void RaiseEvent(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }


        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
