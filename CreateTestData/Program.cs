﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CreateTestData
{
    internal class Program
    {
        private int[] couples = new int[]
            // {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};
            // {1, 3, 4, 5, 7, 8, 10, 11, 12, 16, 17, 18, 19, 20, 22, 23, 24, 25};
            // {1, 3, 5, 7, 10, 11, 12, 16, 19, 22, 23, 24, 25};
            // {1, 3, 5, 7, 11, 16, 25};
            // {61, 62, 63, 64, 65, 67, 68, 69, 71, 72, 73, 74, 76, 78};
            // {1,2,3,4,5,6,7,8,9,10,11,12 };
            // { 101,  102, 103, 104, 105, 106, 107};
            // {101, 102, 106};
        {103, 104, 105, 107};
        // private string[] dances = new string[]{"SB", "CC", "RB", "PD", "JV"};
        private string[] dances = new string[] { "FS" };
        private string[] judges = new string[]{"A","B","C","D","E","F","G", "H", "I", "J", "K", "L" };
        private string[] judgments = new string[]{"7,5", "8", "8,5", "9", "9,5"};

        private void CreateTestData_V1()
        {
            var outStr = new StreamWriter(@"C:\eJudge\Data\1_2\Result_test.txt");

            var rnd = new Random();

            foreach (var judge in judges)
            {
                foreach (var couple in couples)
                {
                    var rand = rnd.Next(0, 4);
                    outStr.WriteLine("{0};SA;{1};{2};{3};{4};{5};{6}",
                        judge, couple,
                        judgments[rand], judgments[rand], judgments[rand],
                        judgments[rand],judgments[rand]
                        );
                }
            }

            outStr.Close();
        }

        private void CreateTestData()
        {
            var outStr = new StreamWriter(@"C:\eJudge\Data\1_3\Result_test.txt");

            var area = 1;
            var rnd = new Random();

            foreach (var judge in judges)
            {
                area++;
                if (area > 4) area = 1;

                foreach (var dance in dances)
                {
                    foreach (var couple in couples)
                    {
                        var rand = rnd.Next(0, 4);
                        outStr.WriteLine(String.Format("{0};{1};{2};{3};{4}"
                            ,judge, dance, couple, area, judgments[rand]));
                        
                    }
                }
            }

            outStr.Close();
        }

        static void Main(string[] args)
        {
            var prg = new Program();
            prg.CreateTestData();
        }
    }
}
