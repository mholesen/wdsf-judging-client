﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Judging_Server.Model;
using Microsoft.Win32;
using  System.IO;

namespace Judging_Server.Dialogs
{
    /// <summary>
    /// Interaction logic for ImportTPS.xaml
    /// </summary>
    public partial class ImportTPS : Window
    {
        private DataContextClass _context;
        private string _token;

        public ImportTPS(DataContextClass context)
        {
            InitializeComponent();
            _context = context;
        }

        private void SelectFile_OnClick(object sender, RoutedEventArgs e)
        {
            var open = new OpenFileDialog();

            var res = open.ShowDialog();
            if (res.HasValue && res.Value)
            {
                FileName.Text = open.FileName;
            }
        }

        private void BtnOK_OnClick(object sender, RoutedEventArgs e)
        {
            if (!System.IO.File.Exists(FileName.Text)) return;

            var stream = new System.IO.StreamReader(FileName.Text);
            ImportData(stream);
            _context.SaveData();
            stream.Close();

            DialogResult = true;
            Close();
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }

        private void ImportData(StreamReader sr)
        {
            _token = "";
            while (!sr.EndOfStream)
            {
                var lastToken = _token;
                if (_token.StartsWith("[TurnierDaten]"))
                    ImportEvent(sr);
                if (_token.StartsWith("[Taenze]"))
                    ImportDances(sr);
                if(_token.StartsWith("[Wertungsrichter]"))
                    ImportJudges(sr);
                if(_token.StartsWith("[StartListe]"))
                    ImportCouples(sr);

                if(_token == lastToken)
                    _token = sr.ReadLine();
            }
        }

        private string GetValue(string line)
        {
            var data = line.Split('=');

            return data.Length > 1 ? data[1] : "";
        }

        private string[] GetTPSData(string line)
        {
            var data = line.Split(';');
            for(int i=0;i < data.Length;i++)
            {
                if (data[i].StartsWith("\""))
                {
                    data[i] = data[i].Substring(1, data[i].Length - 2);
                }
            }

            return data;
        }

        private void ImportDances(StreamReader sr)
        {
            _context.Dances = new List<Dance>();
            var line = sr.ReadLine();
            var count = Int32.Parse(GetValue(line)); // 1. Zeile ist Anzahl (Cnt=x)
            for (int i = 0; i < count; i++)
            {
                line = sr.ReadLine();
                var data = GetTPSData(line);
                var dance = new Dance
                    {
                        LongName = data[1]
                    };

                switch (dance.LongName)
                {
                    case "Waltz":
                        dance.ShortName = "SW";
                        break;
                    case "Tango": dance.ShortName = "TG";
                        break;
                    case "Viennese Waltz":
                        dance.ShortName = "VW";
                        break;
                    case "Slow Foxtrot":
                        dance.ShortName = "SF";
                        break;
                    case "Quickstep":
                        dance.ShortName = "QS";
                        break;
                    case "Samba":
                        dance.ShortName = "SB";
                        break;
                    case "Cha Cha Cha":
                        dance.ShortName = "CC";
                        break;
                    case "Rumba":
                        dance.ShortName = "RB";
                        break;
                    case "Paso doble":
                        dance.ShortName = "PD";
                        break;
                    case "Jive":
                        dance.ShortName = "JV";
                        break;

                }
                _context.Dances.Add(dance);
            }
            _token = "";
        }

        private void ImportEvent(StreamReader sr)
        {
            _context.Event = new EventData();
            while (!sr.EndOfStream)
            {
                var line = sr.ReadLine();
                if (line.StartsWith("["))
                {
                    _token = line;
                    return;
                }
                if (line.StartsWith("TurnierTitel"))
                    _context.Event.Title = GetValue(line);
                if (line.StartsWith("Veranstalter"))
                    _context.Event.Organizer = GetValue(line);
                if (line.StartsWith("TurnierNr"))
                    _context.Event.WDSFId = GetValue(line);
                if (line.StartsWith("Ort"))
                    _context.Event.Place = GetValue(line);
            }
        }

        private void ImportJudges(StreamReader sr)
        {
            _context.Judges = new List<Judge>();


            var line = sr.ReadLine();
            var count = Int32.Parse(GetValue(line)); // 1. Zeile ist Anzahl (Cnt=x)
            for (int i = 0; i < count; i++)
            {
                line = sr.ReadLine();
                var data = GetTPSData(line);
                var judge = new Judge
                    {
                        Name = data[1] + ", " + data[2],
                        Country = data[3],
                        WDSFSign = GetValue(data[0]),
                        Sign = ((char)('A' + ((char) i))).ToString(),
                        MIN = data[4]
                    };
                _context.Judges.Add(judge);
            }
            _token = "";
        }

        private void ImportCouples(StreamReader sr)
        {
            _context.Couples = new List<Couple>();
            var line = sr.ReadLine();
            var count = Int32.Parse(GetValue(line)); // 1. Zeile ist Anzahl (Cnt=x)
            for (int i = 0; i < count; i++)
            {
                line = sr.ReadLine();
                var data = GetTPSData(line);
                if (data[12] == "")
                {
                    var couple = new Couple
                        {
                            Id = Int32.Parse(GetValue(data[0])),
                            Name = data[2] + " " + data[1] + " - " + data[4] + " " + data[3],
                            Country = data[5],
                            WDSF_ID = data[6]
                        };
                    _context.Couples.Add(couple);
                }
            }
            _token = "";
        }
    }
}
