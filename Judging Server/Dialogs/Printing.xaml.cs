﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using CommonLibrary;

using Judging_Server.Helper;
using Judging_Server.Model;
using Judging_Server.Print;

using Scrutinus.Reports;
using Scrutinus.Reports.BasicPrinting;

namespace Judging_Server.Dialogs
{
    public class PrintDlgModel
    {
        public bool PrintDances { get; set; }
        public bool PrintCouples { get; set; }
        public int CopiesDances { get; set; }
        public int CopiesCouples { get; set; }
        public int CopiesResultCouples { get; set; }
        public bool PrintResult { get; set; }
        public bool PrintResultCouples { get; set; }
        public bool PrintQualified { get; set; }
        public bool PrintDroppedOut { get; set; }
        public bool PrintJudgesDrawing { get; set; }
        public bool PrintCouplesMarking { get; set; }
        public int PlaceCutOff { get; set; }
    }
    /// <summary>
    /// Interaction logic for Print.xaml
    /// </summary>
    public partial class Printing : Window
    {
        private PrintDlgModel _model;
        private Model.DataContextClass _context;

        public Printing(Model.DataContextClass context)
        {
            InitializeComponent();
            _model = new PrintDlgModel()
                {
                    PrintDances = true,
                    PrintCouples = true,
                    CopiesDances = 3,
                    CopiesCouples = 1,
                    CopiesResultCouples = 1,
                    PrintResult = false,
                    PrintResultCouples = false,
                    PrintCouplesMarking = false
                };

            DataContext = _model;
            _context = context;
        }

        private void BtnOK_OnClick(object sender, RoutedEventArgs e)
        {
            if (_model.PrintCouplesMarking)
            {
                var report = new CoupleDetailResultPrinter(this._context);

                var reportPage = new PrintReportPage();

                reportPage.Print(report, false, false, null, 1, "Couples Marking");
            }

            if (_model.PrintDances)
            {
                Print.Printer.PrintReport(_context.Event.Title, "Drawing Round " + _context.Event.RoundNr,
                                            "Judging_Server.Print.RoundDrawingNormal.rdlc", ReportModels.ReportModelHelper.getDrawingDataSource(_context),
                                            "HeatsDataSet", _model.CopiesDances);
            }

            if (_model.PrintJudgesDrawing)
            {
                Print.Printer.PrintReport(_context.Event.Title, "Drawing Judges",
                                            "Judging_Server.Print.JudgesDrawing.rdlc", ReportModels.ReportModelHelper.getJudgesDrawingDataSource(_context),
                                            "JudgesDrawingDataSet", _model.CopiesDances);
            }

            if (_model.PrintCouples)
            {
                var model = _context.Couples.Select(s => new ReportModels.CoupleHeats {Number = s.Id}).ToList();
                var dances = _context.HeatsSequence.GroupBy(h => h.Dance).Select(d => d.First()).ToList();

                var parameters = new Dictionary<string, string>
                    {
                        {"Dance1", dances[0].Dance.LongName},
                        {"Dance2", dances[1].Dance.LongName},
                        {"Dance3", dances[2].Dance.LongName},
                        {"Dance4", dances[3].Dance.LongName},
                        {"Dance5", dances[4].Dance.LongName}
                    };

                foreach (var coupleHeatse in model)
                {
                    coupleHeatse.HeatDance_A =
                        _context.HeatsSequence.Single(
                            d =>
                            d.Dance.ShortName == dances[0].Dance.ShortName &&
                            d.Couples.Any(c => c.Id == coupleHeatse.Number)).Id + 1;

                     coupleHeatse.HeatDance_B =
                        _context.HeatsSequence.Single(
                            d =>
                            d.Dance.ShortName == dances[1].Dance.ShortName &&
                            d.Couples.Any(c => c.Id == coupleHeatse.Number)).Id +1;

                     coupleHeatse.HeatDance_C =
                        _context.HeatsSequence.Single(
                            d =>
                            d.Dance.ShortName == dances[2].Dance.ShortName &&
                            d.Couples.Any(c => c.Id == coupleHeatse.Number)).Id + 1;

                     coupleHeatse.HeatDance_D =
                        _context.HeatsSequence.Single(
                            d =>
                            d.Dance.ShortName == dances[3].Dance.ShortName &&
                            d.Couples.Any(c => c.Id == coupleHeatse.Number)).Id + 1;

                     coupleHeatse.HeatDance_E =
                        _context.HeatsSequence.Single(
                            d =>
                            d.Dance.ShortName == dances[4].Dance.ShortName &&
                            d.Couples.Any(c => c.Id == coupleHeatse.Number)).Id + 1;
    
                }

               

                Print.Printer.PrintReport(_context.Event.Title, "Drawing Round " + _context.Event.RoundNr,
                                            "Judging_Server.Print.CoupleHeats.rdlc", model,
                                            "CoupleHeatsDataSet", parameters, _model.CopiesCouples);
            }
            if (_model.PrintResultCouples)
            {
                var model = _context.TotalResult.OrderBy(t => t.Place).Select(s => new ReportModels.ResultPrintModel{Name = "", Number = s.Couple.Id, Place = s.Place, Points = s.Total, ComponentA = s.Result_A.Round2(), ComponentB = s.Result_B.Round2(), ComponentC = s.Result_C.Round2(), ComponentD = s.Result_D.Round2()}).ToList();
                
                foreach (var entry in model)
                {
                    entry.Name = _context.Couples.Single(c => c.Id == entry.Number).NiceName;
                }

                Print.Printer.PrintReport(_context.Event.Title, "Result ordered by Places",
                        "Judging_Server.Print.CouplesOrderedByResult.rdlc", model,
                        "ResultDataSet", _model.CopiesResultCouples);
            }

            if (_model.PrintDroppedOut)
            {
                var model = _context.TotalResult.Where(p => p.Place > _model.PlaceCutOff).OrderBy(t => t.Place).Select(s => new ReportModels.ResultPrintModel { Name = s.Couple.NiceName, Number = s.Couple.Id, Place = s.Place, Points = s.Total }).ToList();
                foreach (var entry in model)
                {
                    entry.Name = _context.Couples.Single(c => c.Id == entry.Number).NiceName;
                }

                Print.Printer.PrintReport(_context.Event.Title, "Couples Dropped out",
                        "Judging_Server.Print.CouplesOrderedByResult.rdlc", model,
                        "ResultDataSet", 1);
            }

            if (_model.PrintQualified)
            {
                var orderedResult = _context.TotalResult.Where(p => p.Place <= _model.PlaceCutOff).OrderBy(t => t.Couple.Id).ToList();
                var model = orderedResult.Select(s => new ReportModels.ResultPrintModel { Name = s.Couple.NiceName, Number = s.Couple.Id, Place = 0, Points = 0 });
                foreach (var entry in model)
                {
                    entry.Name = _context.Couples.Single(c => c.Id == entry.Number).NiceName;
                }

                Print.Printer.PrintReport(_context.Event.Title, "Couples qualified next Round",
                        "Judging_Server.Print.CouplesOrderedByResult.rdlc", model,
                        "ResultDataSet", 1);
            }
        }

        public static void PrintReport(string printerName, int copies, bool preview, AbstractPrinter printer, string printTitle, bool landscape = false)
        {
            var printPage = new PrintReportPage();

            printPage.Print(printer, false, preview, printerName, copies, printTitle, landscape);

        }

    }
}
