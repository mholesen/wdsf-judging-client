﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Xps.Packaging;
using System.Windows.Xps.Serialization;
using System.Windows.Xps;
using Warehouse.Models;
using System.Configuration;

namespace Judging_Server.Print
{
    class Lieferschein : AbstractPrinter
    {
        private DateTime date;
        private List<Models.Artikel> artikel;
        private Models.Adresse adresse;
        private bool shippingToStore;

        private Lieferschein(ServiceModels.ShippingNote shippingNote, bool ShippingToStore)
        {
            this.date = shippingNote.Date.Value;
            artikel = shippingNote.Units.Select(u => new Models.Artikel(u)).ToList();
            if (ShippingToStore)
            {
                artikel.Insert(0, new Models.Artikel("Per Media", "Transportbox", 1, shippingNote.ShippingBoxCode));
            }
            artikel.AddRange(shippingNote.POSMaterials.Select(p => new Models.Artikel(p)));
            this.shippingToStore = ShippingToStore;
        }

        public Lieferschein(ServiceModels.ShippingNote shippingNote, ServiceModels.Store store) : this(shippingNote, true)
        {
            adresse = new Models.Adresse()
            {
                Empfänger = store.Name,
                Land = "D",
                PlzOrt = store.ZipCode + " " + store.City,
                Strasse = store.Street
            };
        }

        public Lieferschein(ServiceModels.ShippingNote shippingNote, ServiceModels.EtailOrder order) : this(shippingNote, false)
        {
            adresse = new Models.Adresse()
            {
                Empfänger = order.FullName,
                Zeile1 = order.Company,
                Land = "D",
                PlzOrt = order.ZipCode + " " + order.City,
                Strasse = order.Street
            };
        }

        /// <summary>
        /// Druckt den Lieferschein. Wenn der Parameter "backupFilenameId" gesetzt ist wird eine
        /// Kopie des Lieferscheins unter dem Namen "backupFilenameId" + ".xps" in dem Verzeichnis
        /// das durch das appSettings "ShippingNote.Path" festgelegt wird gespeichert.
        /// </summary>
        /// <param name="backupFilenameId">Name der Backup Datei</param>
        public void Print(string backupFilenameId = null) 
        {
            CreatePages();
            PageNumbers = true;

            if (!string.IsNullOrWhiteSpace(backupFilenameId))
            {
                // Kopie des Lieferscheins erstellen
                try
                {
                    var shippingNotePath = ConfigurationManager.AppSettings["ShippingNote.Path"];
                    System.IO.Directory.CreateDirectory(shippingNotePath);
                    PrintDocument(shippingNotePath + backupFilenameId + ".xps", false);
                }
                catch (Exception e)
                {
                    App.TraceSource.TraceEvent(System.Diagnostics.TraceEventType.Error, 2, e.Message);
                    ErrorDialog.Show("Es konnte keine Kopie des Lieferscheins gespeichert werden.", e);
                }
            }

            PrintDocument(null, true);
        }

        protected override void AddCommenContent(FixedPage page)
        {
            AddText(page, "Lieferschein", 10, 2, 24);
            // Die Fußzeile hinzufügen
            AddText(page, "Der Notebook-Lieferumfang beinhaltet neben dem Gerät generell auch den jeweils zugehörigen Akku mit gleichlautender\r\nTrackingnummer, ein Netzteil sowie ein Stromkabel.", 22, 1.8, 12);
            // Hinweistext, dass Seite abgehackt werden muss:
            if (shippingToStore)
            {
                AddText(page, "Bei Empfang der Ware sind alle Positionen auf Vollständigkeit zu prüfen und auf dem Beleg abzuhaken! Nach Kontrolle und Unterzeichnung des Lieferscheins\r\nist dieser als Photo aufzunehmen und noch am selben Tag per Upload bereitzustellen!"
                    , 11, 2, 8, 20, 2, HorizontalAlignment.Left, FontWeights.Normal, 0);
            }
        }

        protected void CreatePages()
        {
            FixedPage fixedPage = createPage();
            // Wir fügen die Adresse auf der ersten Seite ein
            AddText(fixedPage, adresse.Empfänger, 4, 2, 14);
            AddText(fixedPage, adresse.Zeile1, 4.6, 2, 14);
            AddText(fixedPage, adresse.Zeile2, 5.2, 2, 14);
            AddText(fixedPage, adresse.Strasse, 5.8, 2, 14);
            AddText(fixedPage, adresse.PlzOrt, 6.6, 2, 14);
            // Ort und Datum
            AddText(fixedPage, "Köln, den " + date.ToShortDateString(), 8, 2, 12);
            // Nun geben wir mal alle Artikel aus
            int lineCount = 0;
            // Überschriftenzeile
            AddText(fixedPage, "Menge", 12, 2, 12, 1.4, 0.5, HorizontalAlignment.Left, FontWeights.Bold, 0);
            AddText(fixedPage, "Hersteller", 12, 3.4, 12, 2.8, 0.5, HorizontalAlignment.Left, FontWeights.Bold, 0);
            AddText(fixedPage, "Art.-Nr. / -Bezeichnung", 12, 6.2, 12, 8, 0.5, HorizontalAlignment.Left, FontWeights.Bold, 0);
            AddText(fixedPage, "Trackingnummer", 12, 14.2, 12, 12, 0.5, HorizontalAlignment.Left, FontWeights.Bold, 0);
            AddHorizontalLine(fixedPage, 11.8, 1.8, 15.4);
            AddHorizontalLine(fixedPage, 12.6, 1.8, 15.4);
            foreach (Models.Artikel a in artikel)
            {
                AddText(fixedPage, a.Anzahl != -1 ? a.Anzahl.ToString() : "Unbek.", 12.7 + 0.7 * lineCount, 2, 12, 1.4, 0.5, HorizontalAlignment.Center, FontWeights.Normal, 0);
                AddText(fixedPage, a.Hersteller, 12.7 + 0.7 * lineCount, 3.4, 12, 2.8, 0.5, HorizontalAlignment.Center, FontWeights.Normal, 0);
                AddText(fixedPage, a.Model, 12.7 + 0.7 * lineCount, 6.2, 12, 8, 0.5, HorizontalAlignment.Left, FontWeights.Normal, 0);
                AddText(fixedPage, a.TrackingNummer, 12.7 + 0.7 * lineCount, 14.2, 12, 8, 0.5, HorizontalAlignment.Left, FontWeights.Normal, 0);
                AddHorizontalLine(fixedPage, 12.7 + 0.7 * lineCount + 0.5, 1.8, 15.4);
                lineCount++;

                if (lineCount > 12)
                {
                    // AddVerticalLine(fixedPage, 12, 3, 4);
                    lineCount = 0; // neue Seite anlegen
                     // Neue Seite erzeugen und Standardwerte setzen
                    AddVerticalLine(fixedPage, 11.8, 1.8, 0.7 * 14);
                    AddVerticalLine(fixedPage, 11.8, 3.2, 0.7 * 14);
                    AddVerticalLine(fixedPage, 11.8, 6.0, 0.7 * 14);
                    AddVerticalLine(fixedPage, 11.8, 14, 0.7 * 14);
                    AddVerticalLine(fixedPage, 11.8, 17.2, 0.7 * 14);
                    // Wir bauen nun mal unsere Seite auf
                    fixedPage = createPage();
                    // Überschriftenzeile
                    AddText(fixedPage, "Menge", 12, 2, 12, 1.4, 0.5, HorizontalAlignment.Left, FontWeights.Bold, 0);
                    AddText(fixedPage, "Hersteller", 12, 3.4, 12, 2.8, 0.5, HorizontalAlignment.Left, FontWeights.Bold, 0);
                    AddText(fixedPage, "Art.-Nr. / -Bezeichnung", 12, 6.2, 12, 8, 0.5, HorizontalAlignment.Left, FontWeights.Bold, 0);
                    AddText(fixedPage, "Bemerkung", 12, 14.2, 12, 3, 0.5, HorizontalAlignment.Left, FontWeights.Bold, 0);
                    AddHorizontalLine(fixedPage, 11.8, 1.8, 15.4);
                    AddHorizontalLine(fixedPage, 12.6, 1.8, 15.4);
                }
            }

            // Noch die Box auf der letzten Seite aufbauen
            AddVerticalLine(fixedPage, 11.8, 1.8, 0.7 * (lineCount + 1));
            AddVerticalLine(fixedPage, 11.8, 3.2, 0.7 * (lineCount + 1));
            AddVerticalLine(fixedPage, 11.8, 6.0, 0.7 * (lineCount + 1));
            AddVerticalLine(fixedPage, 11.8, 14, 0.7 * (lineCount + 1));
            AddVerticalLine(fixedPage, 11.8, 17.2, 0.7 * (lineCount + 1));
        }

    }
}
