﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using Microsoft.Reporting.WinForms;

namespace Judging_Server.Print
{
    public class Printer
    {
        private static Printer _instance;

        public static void PrintReport(string competitionTitle, string reportTitle, string Ressource, object dataSource, string datasetName, int copies = 1)
        {
            if(_instance == null)
                _instance = new Printer();

            _instance.printIt(competitionTitle, reportTitle, Ressource, dataSource, datasetName, null, copies);
        }

        public static void PrintReport(string competitionTitle, string reportTitle, string Ressource, object dataSource, string datasetName, Dictionary<string, string> parameters, int copies = 1)
        {
            if (_instance == null)
                _instance = new Printer();

            _instance.printIt(competitionTitle, reportTitle, Ressource, dataSource, datasetName, parameters, copies);
        }

        public void printIt(string competitionTitle, string reportTitle, string Ressource, object dataSource, string datasetName, Dictionary<string, string> parameters, int copies)
        {
            var report = new LocalReport();
            report.ReportEmbeddedResource = Ressource;
            report.DataSources.Add(
               new ReportDataSource(datasetName, dataSource));

            var parms = new List<ReportParameter>
                {
                    new ReportParameter("CompetitionTitle", competitionTitle),
                    new ReportParameter("ReportTitle", reportTitle)
                };
            if (parameters != null)
            {
                parms.AddRange(parameters.Keys.Select(name => new ReportParameter(name, parameters[name])));
            }

            report.SetParameters(parms);
            Export(report);
            Print(reportTitle, copies);
        }

        private int m_currentPageIndex;
        private IList<Stream> m_streams;

        private Stream CreateStream(string name,
              string fileNameExtension, Encoding encoding,
              string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }
        // Export the given report as an EMF (Enhanced Metafile) file.
        private void Export(LocalReport report)
        {
            string deviceInfo =
              @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                <PageWidth>8.2in</PageWidth>
                <PageHeight>11.6in</PageHeight>
                <MarginTop>0.25in</MarginTop>
                <MarginLeft>0.25in</MarginLeft>
                <MarginRight>0.25in</MarginRight>
                <MarginBottom>0.25in</MarginBottom>
            </DeviceInfo>";
            Warning[] warnings;
            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream,
               out warnings);
            foreach (Stream stream in m_streams)
                stream.Position = 0;
        }
        // Handler for PrintPageEvents
        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(m_streams[m_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            var adjustedRect = new System.Drawing.Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(System.Drawing.Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        private void Print(string reportTitle, int copies)
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");

            var printDoc = new PrintDocument();

            if (!printDoc.PrinterSettings.IsValid)
            {
                throw new Exception("Error: cannot find the default printer.");
            }
            else
            {
                printDoc.DocumentName = reportTitle;
                printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                m_currentPageIndex = 0;
                printDoc.PrinterSettings.Copies = (short) copies;
                printDoc.Print();
            }
        }
    }
}
