﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Judging_Server.Model;

namespace Judging_Server.ReportModels
{
    public class ReportModelHelper
    {
        public static object getDrawingDataSource(Model.DataContextClass context)
        {
            // we create our sort of "Flat" Report Model
            var list = new List<ReportModels.Heat>();
            var order = 0;
            //foreach (var dance in context.Dances)
            foreach (var key in context.Heats.Keys)
            {
                var index = 1;
                foreach (var heat in context.Heats[key])
                {
                    var reportHeat = new ReportModels.Heat()
                    {
                        Dance = context.Dances.Single(d => d.ShortName == key).LongName,
                        HeatNo = index,
                        Order = order,
                        Participants = new List<Couple>()
                    };
                    reportHeat.Participants.AddRange(heat.Couples);
                    list.Add(reportHeat);
                    index++;
                    order++;
                }
            }

            return list;
        }

        public static object getJudgesDrawingDataSource(Model.DataContextClass context)
        {
            var list = new List<ReportModels.JudgesDrawingModel>();
            // lets only print first dance
            var firstDance = context.JudgeDrawing.Keys.First();
            var drawing = context.JudgeDrawing[firstDance];
            foreach (var element in drawing)
            {
                foreach(var judge in element.Judges)
                {
                    list.Add(new ReportModels.JudgesDrawingModel
                    {
                        Dance = firstDance,
                        Heat = element.Heat.Id,
                        Component = context.JudgingAreas[element.Area - 1].Component,
                        JudgeName = judge.Name,
                        Sign = judge.Sign
                    });
                }
            }

            return list.OrderBy(o => o.Heat).ThenBy(o => o.Component);
        }
    }
}
