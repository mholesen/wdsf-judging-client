﻿using System.Net;
using System.Net.Sockets;

using CommonLibrary;

using Judging_Server.Nancy;

namespace Judging_Server
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Timers;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;

    using Judging_Server.Dialogs;
    using Judging_Server.Helper;
    using Judging_Server.Model;
    using Judging_Server.Print;

    using Timer = System.Timers.Timer;

    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ServerMainWindows : Window
    {
        #region Fields

        private readonly Brush _buttonBackground;

        private bool BeamerIsOn;

        private int _lastTVHeat;

        private TimeSpan timeLeft;

        private Timer timer;

        private DateTime timerStartTime;

        private TimeSpan timerRunDown;

        private FileSystemWatcher _watcher;

        private DataContextClass context;

        private Thread autoconfigThread;

        private NancyHttpServer nancyHttpServer;

        #endregion

        #region Constructors and Destructors

        public ServerMainWindows()
        {
            Instance = this;

            this.InitializeComponent();

            this.AutoShowResult = true;

            if (File.Exists(ServerSettings.Default.LastCompetitionPath + "\\data.txt")
                || File.Exists(ServerSettings.Default.LastCompetitionPath + "\\data.xml"))
            {
                this.OpenCompetition(ServerSettings.Default.LastCompetitionPath);
            }

            this.ChkAutoShowResult.DataContext = this;
            this._buttonBackground = this.ShowResultButton_Couple.Background;

            this.PreviewKeyUp += this.MainWindow_PreviewKeyUp;

            autoconfigThread = new Thread(this.ServiceLocalizerThread);
            autoconfigThread.IsBackground = true;
            autoconfigThread.Start();

            AppDomain.CurrentDomain.UnhandledException += this.CurrentDomain_UnhandledException;

            nancyHttpServer = new NancyHttpServer();
            nancyHttpServer.StartHost();
        }

        #endregion

        #region Public Properties

        public bool AutoShowResult { get; set; }

        public DataContextClass Context
        {
            get
            {
                return this.context;
            }
        }

        public static ServerMainWindows Instance { get; private set; }

        #endregion

        #region Methods

        private void BtnChangeData_OnClick(object sender, RoutedEventArgs e)
        {
            var dlg = new ChangeMarking(this.context);
            dlg.ShowDialog();
        }

        private void BtnExport_OnClick(object sender, RoutedEventArgs e)
        {
            this.ExportData();
            MessageBox.Show("Data has been exported");
        }

        private void BtnHeats_OnClick(object sender, RoutedEventArgs e)
        {
            var dlg = new CreateHeats(this.context);
            dlg.ShowDialog();
            if (dlg.DialogResult.HasValue && dlg.DialogResult.Value)
            {
                this.context.CreateSequenceLists();
                // CoupleSequence.ItemsSource = context.HeatsSequence; 
            }
        }

        private void BtnOpen_OnClick(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenCompetition();
            bool? res = dlg.ShowDialog();
            if (res.HasValue && res.Value)
            {
                // Open
                this.OpenCompetition(dlg.SelectedFolder);
                ServerSettings.Default.LastCompetitionPath = dlg.SelectedFolder;
                ServerSettings.Default.Save();
            }
        }

        private void BtnPrint_OnClick(object sender, RoutedEventArgs e)
        {
            var dlg = new Printing(this.context);
            dlg.ShowDialog();
        }

        private bool CheckHeatIsJudges(int heatId)
        {
            if (heatId < 0)
            {
                return false;
            }

            Heat heat = context.HeatsSequence.SingleOrDefault(h => h.Id == heatId);
            if (heat == null)
            {
                return false;
            }

            int marksShould = this.Context.Judges.Count * heat.Couples.Count;
            int count = 0;

            count =
                this.Context.AllJudgements.Count(
                    m =>
                    m.Dance == heat.Dance.ShortName && heat.Couples.Any(c => c.Id == m.Couple)
                    && (m.MarkA > 0 || m.MarkB > 0 || m.MarkC > 0 || m.MarkD > 0));

            return count == marksShould;
        }

        private void Clear_Device_OnClick(object sender, RoutedEventArgs e)
        {
            // Clear and restart Application
            MessageBoxResult res = MessageBox.Show(
                "Are you sure to clear all devices?",
                "Clear Devices?",
                MessageBoxButton.OKCancel);

            if (res == MessageBoxResult.Cancel)
            {
                return;
            }

            foreach (DeviceStates device in this.context.DeviceState)
            {
                try
                {
                    if (device.DevicePath.ToLower().StartsWith("http:/"))
                    {
                        ExportHelpers.WriteToServer(device.DevicePath, "newState.txt", "clear\r\n");
                    }
                    else
                    {
                        string path = device.DevicePath + "\\newState.txt";
                        // Zum testen:
#if DEBUG_LOCAL
                        path = @"C:\eJudge\TestingLocal\newState.txt";
#endif
                        var sw = new StreamWriter(path);
                        sw.WriteLine("clear");
                        sw.Close();
                    }
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void ClickContextMenue(object sender, RoutedEventArgs e)
        {
            if (!(e.Source is MenuItem))
            {
                return;
            }

            var item = (MenuItem)e.Source;

            switch (item.Name)
            {
                case "ReadData":
                    DataContextClass.RaiseEvents = false;
                    IEnumerable<string> files = Directory.EnumerateFiles(
                        ServerSettings.Default.DataPath,
                        "Result_*.txt");
                    foreach (string file in files)
                    {
                        var ino = new FileInfo(file);
                        var ev = new FileSystemEventArgs(
                            WatcherChangeTypes.Created,
                            ServerSettings.Default.DataPath,
                            ino.Name);
                        this.FileSystem_Changed(this, ev);
                    }

                    DataContextClass.RaiseEvents = true;

                    break;
                case "Export":
                    this.ExportData();
                    break;
                case "Print":
                    var doc = new TotalResults();
                    doc.PrintPage(this.context, ServerSettings.Default.DataPath + "\\result.html");
                    break;
                case "Drawing":
                    var rg = new RandomGenerator();
                    rg.Initialize();
                    rg.Draw(this.context.Couples, this.context.Dances);
                    rg.Dump(ServerSettings.Default.DataPath);
                    break;
                case "DataExchange":
                    this.WriteDataExchange();
                    break;
                case "BestWorstAnalysis":
                    var window = new ShowBestWorstAnalysis(this.Context);
                    window.Show();
                    break;
            }
        }

        private void CoupleSequence_OnBeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            e.Cancel = true;
        }

        private void CoupleSequence_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.CoupleSequence.SelectedItem is Heat)
            {
                var selected = (Heat)this.CoupleSequence.SelectedItem;
                this.LblSelectedCouple.Content = String.Format(
                    "Heat {0} - Dance {1}",
                    selected.Id,
                    selected.Dance.ShortName);
            }
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;

            MessageBox.Show(ex.Message);
            if (ex.InnerException != null)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
            MessageBox.Show(ex.StackTrace);
        }

        private void ExportData()
        {
            var writer =
                new StreamWriter(
                    String.Format(
                        "{0}\\{1}_export.csv",
                        ServerSettings.Default.DataPath,
                        DateTime.Now.ToString("yyyyMMdd_HHmmss")));
            if (this.context.CalculationVersion != CalculationVersionEnum.Version1)
            {
                this.context.ExportExcelV2(writer);
            }
            else
            {
                this.context.ExportExcel(writer);
            }

            writer.Close();
        }

        private void FileSystem_Changed(object sender, FileSystemEventArgs e)
        {
            // Wir haben eine neue Datei erhalen und schauen uns die mal an
            StreamReader sr = null;
            // Give the client some time to write the data
            Thread.Sleep(100);

            int errCount = 0;
            while (sr == null && errCount < 30)
            {
                try
                {
                    sr = new StreamReader(e.FullPath);
                }
                catch (Exception)
                {
                    // file might be still open for writing so we just wait
                    // a view milliseconds and try again
                    Thread.Sleep(200);
                    errCount++;
                }
            }

            if (sr == null)
            {
                MessageBox.Show("Could not open file " + e.FullPath);
                return;
            }

            if (e.Name.StartsWith("State"))
            {
                var info = new FileInfo(e.FullPath);
                this.HandleNewState(sr, info.LastWriteTime);
                sr.Close();
                return;
            }

            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();

                string[] data = line.Split(';');
                if (data.Length < 8)
                {
                    this.UpdateMarkingV2(data);
                }
                else
                {
                    this.UpdateMarkingV1(data);
                }
            }
            sr.Close();
        }

        private void HandleNewState(StreamReader sr, DateTime lastChanged)
        {
            // Hier den State bearbeiten
            // ToDo: State bearbeiten
            if (sr.EndOfStream)
            {
                return;
            }
            string[] data = sr.ReadLine().Split(';');
            // Wir suchen uns das Gerät anhand des Judges heraus:
            try
            {
                DeviceStates device = this.context.DeviceState.Single(d => d.Judge == data[0]);
                device.Dance = Int32.Parse(data[2]);
                int heat = 0;
                if (!int.TryParse(data[1], out heat))
                {
                    heat = 0;
                }
                device.Area = Int32.Parse(data[3]);
                device.Heat = heat;
                device.Time = lastChanged;
                sr.Close();
                // Check if all Devices are in same heats ...
                IEnumerable<DeviceStates> inUse = this.context.DeviceState.Where(d => d.Judge != null);
                if (device.Heat != this._lastTVHeat && inUse.All(d => d.Heat == device.Heat))
                {
                    // Check, if we realy have all marks (Device might change state but sends result a moment later, due to async implementation)
                    if (this.CheckHeatIsJudges(device.Heat))
                    {
                        this._lastTVHeat = device.Heat;
                        this.Dispatcher.Invoke((Action)(() => this.ResultAvailable(device.Heat)));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                sr.Close();
            }
        }

        private void HandleNewTime(string timeStr)
        {
            // Erste Zeile ist der Name des Judges
            TimeSpan time = TimeSpan.Parse(timeStr);

            // Nun müssen wir die Uhr starten
            this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    this.TimeLeft.Content = string.Format("{0:00}:{1:00}", this.timeLeft.Minutes, this.timeLeft.Seconds); 
                }));

            this.timerRunDown = time;
            this.timerStartTime = DateTime.Now;

            if (this.timer != null)
            {
                this.timer.Enabled = false;
            }

            this.timer = new Timer { AutoReset = false, Enabled = false, Interval = 1000 };
            this.timer.Elapsed += this.Timer_Elapsed;
            this.timeLeft = time;
            this.timer.Enabled = true;
        }

        private void ImportTPS_OnClick(object sender, RoutedEventArgs e)
        {
            var dlg = new ImportTPS(this.context);
            bool? result = dlg.ShowDialog();
            if (result.HasValue && result.Value)
            {
                // Show Generate Heats ...
                var dlgheats = new CreateHeats(this.context);
                dlgheats.ShowDialog();
                // to make it save: close this so we are forced to restart
                this.Close();
            }
        }

        private void MainWindow_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                this.StartMusicButton_Click(this, null);
            }
            if (e.Key == Key.Back)
            {
                // Clear Beamer & TV
                this.ShowResult_Click(this.ButtonClearBeamer, null);
                this.ShowResult_Click(this.ButtonClearTV, null);
            }
        }

        private void OpenCompetition(string folder)
        {
            try
            {
                this.context = new DataContextClass();

                this.CoupleSequence.DataContext = this.Context;
                this.CoupleCombo.DataContext = this.Context;

                if (File.Exists(folder + "\\data.xml"))
                {
                    this.context.LoadDataXML(folder + "\\data.xml");
                }
                else
                {
                    this.context.LoadData(folder + "\\data.txt");
                }

                if (File.Exists((folder + "\\JSConfiguration.xml")))
                {
                    this.context.LoadJSConfiguration(folder + "\\JSConfiguration.xml");
                }


                if (File.Exists(folder + "\\heats.txt"))
                {
                    this.context.LoadHeats(folder + "\\heats.txt");
                }
                else
                {
                    if (context.Heats == null || context.Heats.Count == 0)
                    {
                        var dlg = new CreateHeats(this.context);
                        bool? res = dlg.ShowDialog();
                        if (res.HasValue && res.Value)
                        {
                            // We have heats, so do the rest
                            this.context.CreateDataModel();
                        }
                    }
                }

                this.context.LoadJudgesDrawing(folder + "\\judgesDrawing.txt");

                // Now, as we have heats, create the data model
                context.CreateDataModel();

                if (folder.ToLower().Contains("l") || folder.ToLower().Contains("s"))
                {
                    TryLoad10DanceResults(folder);
                }

                if (!File.Exists(folder + "\\judgesDrawing.txt"))
                {
                    var judgeDrawer = new CreateJudgesDrawing();

                    if (this.context.IsFormation)
                    {
                        this.context.JudgeDrawing = judgeDrawer.CreateFormation(
                            this.context.JudgingAreas,
                            this.context.Judges,
                            this.context.Heats,
                            3);
                    }
                    else
                    {
                        if (this.context.CalculationVersion != CalculationVersionEnum.Version1)
                        {
                            this.context.JudgeDrawing = judgeDrawer.Create(
                                this.context.JudgingAreas,
                                this.context.Judges,
                                this.context.Heats,
                                3,
                                4);
                        }
                    }

                    var exportHelper = new ExportHelpers();
                    exportHelper.WriteJudgesDrawing(
                        this.context,
                        ServerSettings.Default.DataPath
                        + String.Format(
                            "\\{0}_{1}\\judgesDrawing.txt",
                            this.context.Event.ScrutinusId,
                            this.context.Event.RoundNr));
                    // Now we need to print
                }

                this.context.LoadDevices(ServerSettings.Default.DataPath + "\\devices.txt");

                // Wir laden die alten Ergebnisse ein, sofern vorhanden:
                DataContextClass.RaiseEvents = false;
                IEnumerable<string> files = Directory.EnumerateFiles(folder, "Result_*.txt");
                foreach (string file in files)
                {
                    var ino = new FileInfo(file);
                    var ev = new FileSystemEventArgs(WatcherChangeTypes.Created, folder, ino.Name);
                    this.FileSystem_Changed(this, ev);
                }

                DataContextClass.RaiseEvents = true;

                this.Title = String.Format("{0} - {1}. Round", this.context.Event.Title, this.context.Event.RoundNr);

                this._lastTVHeat = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}\r\n{1}", ex.Message, ex.StackTrace));
                return;
            }

            try
            {
                this.Tab1.Header = this.context.Dances[0].ShortName;
                this.Tab2.Header = this.context.Dances.Count > 1 ? this.context.Dances[1].ShortName : "";
                this.Tab3.Header = this.context.Dances.Count > 2 ? this.context.Dances[2].ShortName : "";
                this.Tab4.Header = this.context.Dances.Count > 3 ? this.context.Dances[3].ShortName : "";
                this.Tab5.Header = this.context.Dances.Count > 4 ? this.context.Dances[4].ShortName : "";
                // wir binden die einzelnen Tänze
                this.Result1.ItemsSource = this.context.DanceResults[this.context.Dances[0].ShortName];
                this.Result2.ItemsSource = this.context.Dances.Count > 1
                                               ? this.context.DanceResults[this.context.Dances[1].ShortName]
                                               : null;
                this.Result3.ItemsSource = this.context.Dances.Count > 2
                                               ? this.context.DanceResults[this.context.Dances[2].ShortName]
                                               : null;
                this.Result4.ItemsSource = this.context.Dances.Count > 3
                                               ? this.context.DanceResults[this.context.Dances[3].ShortName]
                                               : null;
                this.Result5.ItemsSource = this.context.Dances.Count > 4
                                               ? this.context.DanceResults[this.context.Dances[4].ShortName]
                                               : null;


            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            for (int i = 0; i < this.context.Judges.Count; i++)
            {
                this.Result1.Columns[i + 5].Header = this.context.Judges[i].Sign;
                this.Result2.Columns[i + 5].Header = this.context.Judges[i].Sign;
                this.Result3.Columns[i + 5].Header = this.context.Judges[i].Sign;
                this.Result4.Columns[i + 5].Header = this.context.Judges[i].Sign;
                this.Result5.Columns[i + 5].Header = this.context.Judges[i].Sign;
            }

            // Noch eine FileSystem Überwachung einbauen ...
            if (this._watcher != null)
            {
                this._watcher.EnableRaisingEvents = false;
            }

            this._watcher = new FileSystemWatcher(folder, "*.txt");
            this._watcher.Created += this.FileSystem_Changed;
            this._watcher.Changed += this.FileSystem_Changed;
            this._watcher.EnableRaisingEvents = true;

            this.DeviceStates.ItemsSource = this.context.DeviceState;

            this.TotalResult.ItemsSource = this.context.TotalResult;
        }

        private void TryLoad10DanceResults(string folder)
        {
            var otherFolderName = "";
            if (folder.ToLower().Contains("s"))
            {
                otherFolderName = folder.ToLower().Replace("s", "l");
            }
            else
            {
                otherFolderName = folder.ToLower().Replace("l", "s");
            }

            if (!File.Exists(otherFolderName + "\\Result.csv"))
            {
                return;
            }

            // load the file:
            var stream = new StreamReader(otherFolderName + "\\Result.csv");

            // Skipp the header of the file
            stream.ReadLine();

            while (!stream.EndOfStream)
            {
                var data = stream.ReadLine().Split(',');

                var number = int.Parse(data[0]);
                var points = data[4].ParseString();

                var couple = context.Couples.SingleOrDefault(c => c.Id == number);

                if (couple == null)
                {
                    MessageBox.Show(
                        "Could not find couple with number " + number + " while loading results in 10 Dance");

                    continue;
                }

                couple.PointOffset = points;

                var total = context.TotalResult.SingleOrDefault(c => c.Couple.Id == couple.Id);
                total.Total = couple.PointOffset;
            }

            MessageBox.Show("Loaded results for 10 dance from " + otherFolderName);
        }

        private void ResultAvailable(int heat)
        {
            if (!this.AutoShowResult || heat < 1)
            {
                return;
            }
            // Wir versuchen das Paar zu finden, die Liste zu selektieren und dann das
            // Ergebnis anzuzeigen...
            Heat entry = this.context.HeatsSequence.SingleOrDefault(h => h.Id == heat);
            if (entry != null)
            {
                this.CoupleSequence.SelectedItem = entry;
                // Nun die anzeige Starten...
                this.ShowResult_Click(this.TVSendButton, null);
                this.WDSGSendButton_OnClick(this.WDSGSendButton, null);
            }
        }

        private void SendData_Clicked(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show(
                "Are you sure to send the data to the judges?",
                "Send data?",
                MessageBoxButton.OKCancel);

            if (res == MessageBoxResult.Cancel)
            {
                return;
            }
            // To be tested
            if (this.context.IsFormation)
            {
                foreach (DeviceStates device in this.context.DeviceState)
                {
                    device.Judge = null;
                }

                List<JudgesDrawing> drawing = this.context.JudgeDrawing[this.context.JudgeDrawing.Keys.First()];
                IEnumerable<JudgesDrawing> firstHeat = drawing.Where(d => d.Heat.Id == 1);
                int counter1 = 0;
                int counter2 = 0;
                List<DeviceStates> devicesDown = this.context.DeviceState.Where(d => d.Position == "0").ToList();
                List<DeviceStates> devicesUp = this.context.DeviceState.Where(d => d.Position == "1").ToList();
                foreach (JudgesDrawing judgesDrawing in firstHeat)
                {
                    if (judgesDrawing.Area == 1 || judgesDrawing.Area == 2)
                    {
                        foreach (Judge judge in judgesDrawing.Judges)
                        {
                            devicesDown[counter1].Judge = judge.Sign;
                            counter1++;
                        }
                    }
                    else
                    {
                        foreach (Judge judge in judgesDrawing.Judges)
                        {
                            devicesUp[counter2].Judge = judge.Sign;
                            counter2++;
                        }
                    }
                }
            }

            if (this.context.Judges.Count < 12 && this.context.DeviceState.Count > 11)
            {
                foreach (DeviceStates device in this.context.DeviceState)
                {
                    device.Judge = null;
                }

                int offset = (this.context.DeviceState.Count - this.context.Judges.Count) / 2;

                for (int i = 0; i < this.context.Judges.Count; i++)
                {
                    this.context.DeviceState[i + offset].Judge = this.context.Judges[i].Sign;
                }
            }
            // Hier schreiben wir nun die Daten an alle Clients raus
            var helper = new ExportHelpers();

            foreach (DeviceStates device in this.context.DeviceState)
            {
                try
                {
#if DEBUG_LOCAL
                    path = @"C:\eJudge\TestingLocal\dance.txt";
#endif
                    if (device.Judge != null)
                    {
                        helper.WriteJudgeData(this.context, device.DevicePath, "dance.txt", device);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void SetCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = this.CoupleCombo.SelectedValue != null;
        }

        private void SetTVState(string state)
        {
            this.Dispatcher.BeginInvoke(
                DispatcherPriority.Input,
                new ThreadStart(() => { this.TVState.Content = state; }));
        }

        private void Set_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show(
                "Are you sure to set the judges to this heat?",
                "Send data?",
                MessageBoxButton.OKCancel);
            if (res == MessageBoxResult.Cancel)
            {
                return;
            }

            // Get the heat-Id:
            var heat = (Heat)this.CoupleCombo.SelectedItem;
            if (heat == null)
            {
                MessageBox.Show("Please select a heat");
                return;
            }

            foreach (DeviceStates device in this.context.DeviceState)
            {
                string path = device.DevicePath;
#if DEBUG_LOCAL
                path = @"C:\eJudge\TestingLocal\newState.txt";
#endif
                
                var task = new Task(
                    () =>
                        {
                            try
                            {
                                if (path.ToLower().StartsWith("http://"))
                                {
                                    // Our heat-id is 1 based, the clients 0 based:
                                    ExportHelpers.WriteToServer(path, "newState.txt", (heat.Id - 1).ToString());
                                }
                                else
                                {
                                    var sw = new StreamWriter(path + "\\newState.txt");
                                    // Our heat-id is 1 based, the clients 0 based:
                                    sw.WriteLine((heat.Id - 1).ToString());
                                    sw.Close();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        });
                task.Start();
            }
        }

        private void ShowResult_Click(object sender, RoutedEventArgs e)
        {
            if (sender == this.TVQual)
            {
                var task = new Task(
                    item =>
                        {
                            // Write Starting list to WDSG folder for David ...
                            StreamWriter outs;
                            try
                            {
                                outs = new StreamWriter(ServerSettings.Default.WDSGExport + "\\Couples.csv");
                            }
                            catch (Exception exception)
                            {
                                MessageBox.Show("Could not write couplesList: " + exception.Message);
                                throw;
                            }

                            foreach (var couple in this.context.Couples)
                            {
                                var names = couple.Name.Split('/');

                                var namesMan = names[0].Split('_');
                                if (namesMan.Length == 1)
                                {
                                    namesMan = names[0].Split(' ');
                                }
                                var namesWoman = names[1].Split('_');
                                if (namesWoman.Length == 1)
                                {
                                    namesWoman = names[1].Split(' ');
                                }

                                outs.WriteLine(
                                    "{0};{1};{2};{3};{4};{5};{6}",
                                    couple.Id,
                                    namesMan[0],
                                    namesMan[1],
                                    namesWoman[0],
                                    namesWoman[1],
                                    couple.Country,
                                    string.Format("{0} {1} - {2} {3}", namesMan[0], namesMan[1], namesWoman[0], namesWoman[1])
                                    );

                            }

                            outs.Close();

                            string[] paths = ServerSettings.Default.ResultTVPath.Split(';');
                            foreach (string path in paths)
                            {
                                try
                                {
                                    var sw = new StreamWriter(path + "\\Qualified.txt", false);
                                    foreach (Couple couple in this.context.Couples.OrderBy(c => c.Id))
                                    {
                                        sw.WriteLine("{0};{1};{2}", couple.Id, couple.Name, couple.Country);
                                    }
                                    sw.Close();
                                    sw = new StreamWriter(path + "\\NewQualified.dat", false);
                                    sw.Close();
                                }
                                catch (Exception ex)
                                {
                                    Debug.WriteLine(ex.Message);
                                }
                            }
                        },
                    null); // no aditional Parameter

                task.Start();
            }

            if (sender == this.ShowResultButton_Couple || sender == this.TVSendButton)
            {
                if (this.CoupleSequence.SelectedItem == null)
                {
                    return;
                }
                this.BeamerIsOn = true;
                var button = (Button)sender;
                button.Background = new SolidColorBrush(Colors.LawnGreen);
                // Den Rest machen wir asynchron, damit unsere UI nicht hängt, falls der Netzwerkpfad nicht gefunden werden kann
                var task = new Task(
                    item =>
                        {
                            var lPaths = new List<string>();
                            if (sender == this.ShowResultButton_Couple)
                            {
                                string[] paths = ServerSettings.Default.ResultExportPaths.Split(';');
                                lPaths.AddRange(paths);
                            }
                            else
                            {
                                string[] paths = ServerSettings.Default.ResultTVPath.Split(';');
                                lPaths.AddRange(paths);
                            }

                            foreach (string path in lPaths)
                            {
                                try
                                {
                                    var sr = new StreamWriter(string.Format("{0}\\DanceResult.txt",path, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second));
                                    var dr = (Heat)item;

                                    DanceResultBase result =
                                        this.context.DanceResults[dr.Dance.ShortName].Single(
                                            r => r.Couple.Id == dr.Couples[0].Id);

                                    this.WriteCoupleResult(sr, result, this.context.PlaceOffset);
                                    sr.Close();

                                    File.CreateText(path + "\\NewDanceResult.dat").Close();

                                    this.SetTVState(result.Couple.Id.ToString());
                                }
                                catch (Exception ex)
                                {
                                    Debug.WriteLine(ex.Message);
                                }
                            }
                        },
                    this.CoupleSequence.SelectedItem);
                task.Start();
            }

            if (sender == this.ShowResultButton_Dance || sender == this.TVSendButton)
            {
                if (this.CoupleSequence.SelectedItem == null)
                {
                    return;
                }
                this.BeamerIsOn = true;
                var task = new Task(
                    item =>
                        {
                            var lPaths = new List<string>();
                            if (sender == this.ShowResultButton_Dance)
                            {
                                string[] paths = ServerSettings.Default.ResultExportPaths.Split(';');
                                lPaths.AddRange(paths);
                            }
                            else
                            {
                                string[] paths = ServerSettings.Default.ResultTVPath.Split(';');
                                lPaths.AddRange(paths);
                            }

                            foreach (string path in lPaths)
                            {
                                try
                                {
                                    var sr = new StreamWriter(string.Format("{0}\\DanceTotal.txt", path, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second));
                                    var dr = (Heat)item;
                                    // Wir suchen uns den Passenden Tanz
                                    this.SetTVState(dr.Dance.ShortName);
                                    this.WriteDanceResult(sr, dr.Dance.ShortName, this.context.PlaceOffset);
                                    sr.Close();
                                    File.CreateText(path + "\\NewDanceTotal.dat").Close();
                                }
                                catch (Exception ex)
                                {
                                    Debug.WriteLine(ex.Message);
                                }
                            }
                        },
                    this.CoupleSequence.SelectedItem);
                task.Start();
            }

            if (sender == this.ShowResultButton_Total || sender == this.TVSendButton)
            {
                this.BeamerIsOn = true;
                this.SetTVState("Total");
                var task = new Task(
                    () =>
                        {
                            var lPaths = new List<string>();
                            if (sender == this.ShowResultButton_Total)
                            {
                                string[] paths = ServerSettings.Default.ResultExportPaths.Split(';');
                                lPaths.AddRange(paths);
                            }
                            else
                            {
                                string[] paths = ServerSettings.Default.ResultTVPath.Split(';');
                                lPaths.AddRange(paths);
                            }

                            foreach (string path in lPaths)
                            {
                                try
                                {
                                    var sr = new StreamWriter(string.Format("{0}\\TotalResult.txt", path, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second));
                                    IOrderedEnumerable<DanceResultBase> total =
                                        this.context.TotalResult.OrderBy(t => t.Place).ThenBy(c => c.Couple.Id);
                                    foreach (DanceResultBase danceResult in total)
                                    {
                                        Couple c = this.context.Couples.First(co => co.Id == danceResult.Couple.Id);
                                        sr.WriteLine(
                                            "{0};{1:#0.00};{4};{2};{3}",
                                            (danceResult.Place + this.context.PlaceOffset),
                                            danceResult.Total,
                                            c.Name,
                                            c.Country,
                                            c.Id);
                                    }
                                    sr.Close();
                                    File.CreateText(path + "\\NewTotalResult.dat").Close();
                                }
                                catch (Exception ex)
                                {
                                    Debug.WriteLine(ex.Message);
                                }
                            }
                        });
                task.Start();
            }

            if (sender == this.ButtonClearBeamer)
            {
                this.BeamerIsOn = false;
                // wir müssen Beamer-Control anweisen, den Beamer zu löschen
                string[] paths = ServerSettings.Default.ResultExportPaths.Split(';');
                foreach (string path in paths)
                {
                    try
                    {
                        var sr = new StreamWriter(path + "\\ClearResult.txt");
                        sr.WriteLine("clear");
                        sr.Close();
                    }
                    catch (Exception)
                    {
                        // Do nothing, might be network problem
                    }
                }
                this.ShowResultButton_Couple.Background = this._buttonBackground;
            }

            if (sender == this.ButtonClearTV)
            {
                this.BeamerIsOn = false;
                this.TVSendButton.Background = this._buttonBackground;
                this.WDSGSendButton.Background = this._buttonBackground;

                if (ServerSettings.Default.ResultTVPath.Contains(";"))
                {
                    return;
                }

                // wir müssen Beamer-Control anweisen, den Beamer zu löschen
                try
                {
                    var sr = new StreamWriter(ServerSettings.Default.ResultTVPath + "\\ClearResult.txt");
                    sr.WriteLine("clear");
                    sr.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void StartMusicButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string timer = "00:01:15";

                if (this.TimerSelection.SelectedItem is ComboBoxItem)
                {
                    var item = (ComboBoxItem)this.TimerSelection.SelectedItem;
                    timer = item.Content.ToString();
                }

                this.HandleNewTime(timer);

                foreach (DeviceStates device in this.context.DeviceState.Where(d => d.Judge != null))
                {
                    // Sicherheitshalber Asynchron die Uhrzeit starten
                    // Falls das Netzwerk klemmt ...
#if DEBUG_LOCAL
                    var dsw = new WriteTimeWorker(@"c:\eJudge\TestingLocal\time.txt", timer);
#else
                    
#endif
                    var task = new Task(new Action(() => WriteTime(timer, device.DevicePath)));
                    task.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void WriteTime(string timer, string path)
        {
            try
            {
                if (path.ToLower().StartsWith("http://"))
                {
                    ExportHelpers.WriteToServer(path, "time.txt", timer);
                }
                else
                {
                    var stream = new StreamWriter(path + "\\time.txt");
                    stream.WriteLine(timer);
                    stream.Close();
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
            }
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.timeLeft = this.timerStartTime.Subtract(DateTime.Now).Add(this.timerRunDown);

            this.Dispatcher.BeginInvoke(
                (Action)(() =>
                    {
                        this.TimeLeft.Content = string.Format("{0:00}:{1:00}", this.timeLeft.Minutes, this.timeLeft.Seconds);
                        this.TimeLeft.Foreground = this.timeLeft.TotalSeconds < 15 ? new SolidColorBrush(Color.FromArgb(255, 255, 0, 0)) : new SolidColorBrush(Colors.Black);
                    }));

            if (this.timeLeft.TotalSeconds > 0)
            {
                this.timer.Enabled = true;
            }

            if (this.timeLeft.TotalSeconds < 30 && this.timeLeft.TotalSeconds > 15 && this.BeamerIsOn)
            {
                // Clear Projector and TV Output if still on...
                this.Dispatcher.BeginInvoke(
                    (Action)(() =>
                        {
                            this.ShowResult_Click(this.ButtonClearBeamer, null);
                            this.ShowResult_Click(this.ButtonClearTV, null);
                        }));
            }
        }

        private void UpdateMarkingV1(string[] data)
        {
            // Datenformat: judge;dance;couple;A;B;C;D;E
            // -1: This area has not been judged by the judge
            int couple = Int32.Parse(data[2]);
            string dance = data[1];

            this.context.UpdateJudgement(
                data[0],
                dance,
                couple,
                0, 1,
                data[3].ParseString(),
                data[4].ParseString(),
                data[5].ParseString(),
                data[6].ParseString(),
                data[7].ParseString());
        }

        private void UpdateMarkingV2(string[] data)
        {
            if (data.Length < 4)
            {
                return;
            }
            // Datenformat: judge;dance;couple;Area;Mark
            // -1: This area has not been judged by the judge
            int couple = Int32.Parse(data[2]);
            string dance = data[1];

            int area = Int32.Parse(data[3]);
            double mark = data[4].ParseString(); 

            switch (area)
            {
                case 1:
                    this.context.UpdateJudgement(data[0], dance, couple, area, mark, mark, -1, -1, -1, -1);
                    break;
                case 2:
                    this.context.UpdateJudgement(data[0], dance, couple, area, mark, -1, mark, -1, -1, -1);
                    break;
                case 3:
                    this.context.UpdateJudgement(data[0], dance, couple, area, mark, -1, -1, mark, -1, -1);
                    break;
                case 4:
                    this.context.UpdateJudgement(data[0], dance, couple, area, mark, -1, -1, -1, mark, -1);
                    break;
                    // 5 is not used any more but who knows what will happen in the future
                case 5:
                case 6:
                case 7:
                    this.context.UpdateJudgement(data[0], dance, couple, area, mark, -1, -1, -1, -1, mark);
                    break;
            }
        }

        private void WDSGScoresAllCouples(string fileName)
        {
            if (!ServerSettings.Default.DoWDSGExport)
            {
                return;
            }

            try
            {
                var fileInfo = new FileInfo(fileName);

                if (!Directory.Exists(fileInfo.Directory.FullName))
                {
                    Directory.CreateDirectory(fileInfo.Directory.FullName);
                }

                var sr = new StreamWriter(fileName, false, Encoding.UTF8);

                // Write Headers:
                sr.Write("start number,rank,name,country,score");
                foreach (JudgingArea area in this.context.JudgingAreas)
                {
                    sr.Write(",{0}", area.Component);
                }
                sr.WriteLine("");

                foreach (Couple couple in this.context.Couples)
                {
                    DanceResultBase totalResult = this.context.TotalResult.Single(c => c.Couple.Id == couple.Id);

                    double ra = this.context.AllResults.Where(p => p.Couple == totalResult.Couple).Sum(s => s.Result_A);
                    totalResult.Result_A = ra;
                    double rb = this.context.AllResults.Where(p => p.Couple == totalResult.Couple).Sum(s => s.Result_B);
                    totalResult.Result_B = rb;
                    double rc = this.context.AllResults.Where(p => p.Couple == totalResult.Couple).Sum(s => s.Result_C);
                    totalResult.Result_C = rc;
                    double rd = this.context.AllResults.Where(p => p.Couple == totalResult.Couple).Sum(s => s.Result_D);
                    totalResult.Result_D = rd;

                    sr.WriteLine(
                        "{0},{1},{2},{3},{4},{5},{6},{7},{8}",
                        totalResult.Couple.Id,
                        (totalResult.Place + this.context.PlaceOffset),
                        couple.Name,
                        couple.Country,
                        totalResult.Total.ToString("#0.00").Replace(',', '.'),
                        totalResult.Result_A.ToString("#0.00").Replace(',', '.'),
                        totalResult.Result_B.ToString("#0.00").Replace(',', '.'),
                        totalResult.Result_C.ToString("#0.00").Replace(',', '.'),
                        totalResult.Result_D.ToString("#0.00").Replace(',', '.'));
                }

                sr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("WDSG data could not been written: " + ex.Message);
            }
        }

        private void WDSGSendButton_OnClick(object sender, RoutedEventArgs e)
        {
            var task = new Task(
                item =>
                    {
                        
                        this.WDSGScoresAllCouples(context.WDSGFolder + "\\Scoring.csv");

                        // Write the current standing
                        string path = context.WDSGFolder + "\\Current";
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        path += "\\data.csv";

                        var dr = (Heat)item;
                        if (dr == null)
                        {
                            MessageBox.Show("Select a heat!");
                            return;
                        }
                        
                        DanceResultBase result = this.context.DanceResults[dr.Dance.ShortName].Single(r => r.Couple.Id == dr.Couples[0].Id);

                        // Check if we have all markings we need:
                        if (
                            !context.AllJudgements.Any() ||
                            context.AllJudgements.Any(
                                j =>
                                j.Dance == result.Dance && j.Couple == result.Couple.Id
                                && (j.MarkA == 0.0 || j.MarkB == 0.0 || j.MarkC == 0.0 || j.MarkD == 0.0)))
                        {
                            var msgResult = MessageBox.Show(
                                "Not all judgements are available. Do you want to continue?",
                                "Continue?",
                                MessageBoxButton.YesNo,
                                MessageBoxImage.Question);
                            if (msgResult == MessageBoxResult.No)
                            {   // User requested not to save the data.
                                return;
                            }
                        }

                        var sr = new StreamWriter(path, false, Encoding.UTF8);

                        // Write Headers:
                        sr.Write("start number,rank,name,country,score");
                        foreach (JudgingArea area in this.context.JudgingAreas)
                        {
                            sr.Write(",{0}", area.Component);
                        }
                        sr.WriteLine("");
                        this.WriteWDSGCoupleResult(sr, result, this.context.PlaceOffset);
                        sr.Close();

                        var roundNumber = 0;
                        int.TryParse(context.Event.RoundNr, out roundNumber);

                        var roundName = context.IsFinal ? "Final" : context.Couples.Count > 8 && context.Couples.Count < 15 && roundNumber > 1 ? "Semi final" : "Round " + context.Event.RoundNr;

                        path = this.context.WDSGFolder + "\\" + roundName + "\\" + dr.Dance.LongName;

                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        sr = new StreamWriter(path + "\\data.csv", false, Encoding.UTF8);
                        sr.WriteLine("start number,rank,name,country,score");
                        this.WriteWDSGDanceResult(sr, dr.Dance.ShortName, this.context.PlaceOffset);
                        sr.Close();

                        // Export Overall
                        path = context.WDSGFolder + "\\Overall";

                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        this.WriteOverallResult(path + "\\data.csv");

                        // Write again to the local data folder
                        path = string.Format(
                                    "{0}\\{1}_{2}\\Result.csv",
                                    ServerSettings.Default.DataPath,
                                    Context.Event.ScrutinusId,
                                    context.Event.RoundNr);

                        this.WriteOverallResult(path);

                        this.SetTVState(result.Couple.Id.ToString());

                        this.Dispatcher.Invoke(
                            new Action(
                                () => { this.WDSGSendButton.Background = new SolidColorBrush(Colors.LawnGreen); }));
                    },
                this.CoupleSequence.SelectedItem);
            task.Start();
        }

        private void WriteOverallResult(string path)
        {
            StreamWriter sr;
            sr = new StreamWriter(path, false, Encoding.UTF8);
            sr.WriteLine("start number,rank,name,country,score");
            IOrderedEnumerable<DanceResultBase> total = this.context.TotalResult.OrderBy(t => t.Place);

            foreach (DanceResultBase danceResult in total)
            {
                Couple c = this.context.Couples.First(co => co.Id == danceResult.Couple.Id);
                sr.WriteLine(
                    "{0},{1},{2},{3},{4}",
                    c.Id,
                    danceResult.Total > 0 ? (danceResult.Place + this.context.PlaceOffset) : 0,
                    c.Name.Replace("/", " / "),
                    c.Country,
                    String.Format("{0:0.00}", danceResult.Total).Replace(',', '.'));
            }
            sr.Close();
        }

        private void WriteCoupleResult(StreamWriter writer, DanceResultBase result, int PlaceOffset)
        {
            // Nun hauen wir das Ergebnis raus, von allen WR's, die gewertet haben
            // Aufpassen, da nicht alle WR's da sein müssen. Dabei jeweils den Status mit ausgeben
            // wegen Streichwertung
            Couple c = this.context.Couples.Single(co => co.Id == result.Couple.Id);
            Dance danceObj = this.context.Dances.First(d => d.ShortName == result.Dance);
            int index = this.context.Dances.IndexOf(danceObj) + 1;
            writer.WriteLine(index + ";" + danceObj.LongName);
            // Paar
            writer.WriteLine(result.Couple.Id + ";" + c.Name + ";" + c.Country);

            //Wir brauchen die Ergebnisse aufgespaltet nach den 4 Wertungsgebieten:
            string marks = String.Format(
                "{0};{1};{2};{3}",
                result.Result_A.ToString("#.00"),
                result.Result_B.ToString("#.00"),
                result.Result_C.ToString("#.00"),
                result.Result_D.ToString("#.00"));
            writer.WriteLine(marks.Replace(',', '.'));

            writer.WriteLine(result.Total.ToString("#0.00").Replace(',', '.') + ";" + (result.Place + PlaceOffset));
        }

        private void WriteDanceResult(StreamWriter writer, string dance, int PlaceOffset)
        {
            // Hier schreiben wir nun für alle Paare die bisher getanzt haben
            // Das Ergebnis für den aktuellen Tanz
            List<DanceResultBase> results = this.context.DanceResults[dance];
            IOrderedEnumerable<DanceResultBase> sorted = results.OrderBy(r => r.Place).ThenBy(r => r.Couple.Id);
            Dance danceObj = this.context.Dances.First(d => d.ShortName == dance);
            int index = this.context.Dances.IndexOf(danceObj) + 1;
            writer.WriteLine(index + ";" + danceObj.LongName);

            foreach (DanceResultBase danceResult in sorted)
            {
                // Wir schreiben nur, wenn auch Wertungen vorhanden sind. Zur vereinfachung prüfen wir nur
                // die beiden ersten WR's
                if (danceResult.ResultsPerJudge[this.context.Judges[0].Sign].Total > 0
                    && danceResult.ResultsPerJudge[this.context.Judges[1].Sign].Total > 0)
                {
                    Couple c = this.context.Couples.First(co => co.Id == danceResult.Couple.Id);
                    writer.WriteLine(
                        "{0};{3:#0.00};{4};{1};{2}",
                        (danceResult.Place + PlaceOffset),
                        c.Name,
                        c.Country,
                        danceResult.Total,
                        c.Id);
                }
            }
        }

        private void WriteDataExchange()
        {
            var wr =
                new StreamWriter(
                    String.Format(
                        "{0}\\{1}_dataexchange.csv",
                        ServerSettings.Default.DataPath,
                        DateTime.Now.ToString("yyyyMMdd_HHmmss")));

            wr.WriteLine(this.context.Event.Title);
            wr.WriteLine(this.context.Event.Date);
            wr.WriteLine("1.0"); // Version String

            foreach (Dance dance in this.context.Dances)
            {
                foreach (Couple couple in this.context.Couples)
                {
                    wr.Write("{0};{1};{2};", dance.ShortName, couple.Id, this.context.Judges.Count);
                    foreach (Judge judge in this.context.Judges)
                    {
                        Judgements j = this.context.judgements[dance.ShortName][judge.Sign][couple.Id];
                        wr.Write("{0};{1};{2};{3};{4};{5};", judge.Sign, j.MarkA, j.MarkB, j.MarkC, j.MarkD, j.MarkE);
                    }
                    DanceResultBase res =
                        this.context.AllResults.FirstOrDefault(r => r.Couple.Id == couple.Id && r.Dance == dance.ShortName);
                    wr.WriteLine("{0};{1};{2}", res.ChairmanReduction, res.Total, res.Place);
                }
            }
        }

        private void WriteWDSGCoupleResult(StreamWriter writer, DanceResultBase result, int PlaceOffset)
        {
            // Nun hauen wir das Ergebnis raus, von allen WR's, die gewertet haben
            // Aufpassen, da nicht alle WR's da sein müssen. Dabei jeweils den Status mit ausgeben
            // wegen Streichwertung
            Couple c = this.context.Couples.Single(co => co.Id == result.Couple.Id);
            Dance danceObj = this.context.Dances.First(d => d.ShortName == result.Dance);
            int index = this.context.Dances.IndexOf(danceObj) + 1;

            if (danceObj.LongName == "Caribbean Show")
            {
                double resultA = this.context.AllResults.Where(d => d.Couple.Id == c.Id).Sum(r => r.Result_A);
                double resultB = this.context.AllResults.Where(d => d.Couple.Id == c.Id).Sum(r => r.Result_B);
                double resultC = this.context.AllResults.Where(d => d.Couple.Id == c.Id).Sum(r => r.Result_C);
                double total = resultA + resultB + resultC + result.Result_D;

                writer.WriteLine(
                    "{0},{1},{2},{3},{4},{5},{6},{7},{8}",
                    c.Id,
                    (result.Place + PlaceOffset),
                    c.Name,
                    c.Country,
                    total.ToString("#0.00").Replace(',', '.'),
                    resultA.ToString("#0.00").Replace(',', '.'),
                    resultB.ToString("#0.00").Replace(',', '.'),
                    resultC.ToString("#0.00").Replace(',', '.'),
                    result.Result_D.ToString("#0.00").Replace(',', '.'));
            }
            else
            {
                writer.WriteLine(
                    "{0},{1},{2},{3},{4},{5},{6},{7},{8}",
                    c.Id,
                    (result.Place + this.context.PlaceOffset),
                    c.Name.Replace("/", " / "),
                    c.Country,
                    result.Total.ToString("#0.00").Replace(',', '.'),
                    result.Result_A.ToString("#0.00").Replace(',', '.'),
                    result.Result_B.ToString("#0.00").Replace(',', '.'),
                    result.Result_C.ToString("#0.00").Replace(',', '.'),
                    result.Result_D.ToString("#0.00").Replace(',', '.'));
            }
        }

        private void WriteWDSGDanceResult(StreamWriter writer, string dance, int PlaceOffset)
        {
            // Hier schreiben wir nun für alle Paare die bisher getanzt haben
            // Das Ergebnis für den aktuellen Tanz
            List<DanceResultBase> results = this.context.DanceResults[dance];
            IOrderedEnumerable<DanceResultBase> sorted = results.OrderBy(r => r.Place);
            Dance danceObj = this.context.Dances.First(d => d.ShortName == dance);
            int index = this.context.Dances.IndexOf(danceObj) + 1;

            foreach (DanceResultBase danceResult in sorted)
            {
                Couple c = this.context.Couples.First(co => co.Id == danceResult.Couple.Id);
                writer.WriteLine(
                    "{0},{1},{2},{3},{4}",
                    c.Id,
                    danceResult.Total > 0 ? (danceResult.Place + this.context.PlaceOffset) : 0,
                    c.Name.Replace("/", " / "),
                    c.Country,
                    danceResult.Total.ToString("#0.00").Replace(',', '.'));
            }
        }

        private void ServiceLocalizerThread()
        {

            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            UdpClient udpClient;

            try
            {
                udpClient = new UdpClient(new IPEndPoint(IPAddress.Any, 9123));
            }
            catch (Exception)
            {
                MessageBox.Show("Could not bind Port 9123 for auto-configuration.");
                return;
            }

            while (true)
            {
                try
                {
                    var buffer = udpClient.Receive(ref sender);
                    Console.WriteLine("Received data from " + sender.Address.ToString());
                    // We send something back to the sender so the sender knows us:
                    var updSender = new UdpClient();
                    var bytes = Encoding.ASCII.GetBytes("Wellcome to eJudge");
                    updSender.Send(bytes, bytes.Length, sender);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    // continue
                }
                
            }

        }

        #endregion

        private class WriteStatus
        {
            #region Fields

            private readonly string MyPath;

            private readonly int index;

            #endregion

            #region Constructors and Destructors

            public WriteStatus(string path, int heatIndex)
            {
                this.MyPath = path;
                this.index = heatIndex;
            }

            #endregion

            #region Public Methods and Operators

            public void Work()
            {
                try
                {
                    var sw = new StreamWriter(this.MyPath);
                    sw.WriteLine(this.index);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            #endregion
        }

        private class WriteTimeWorker
        {
            #region Fields

            private readonly string MyPath;

            private readonly string _time;

            #endregion

            #region Constructors and Destructors

            public WriteTimeWorker(string path, string time)
            {
                this.MyPath = path;
                this._time = time;
            }

            #endregion

            #region Public Methods and Operators

            public void Work()
            {
                try
                {
                    
                    var writer = new StreamWriter(this.MyPath);
                    writer.WriteLine(this._time);
                    writer.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            #endregion
        }
    }
}