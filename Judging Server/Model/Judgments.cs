﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Judging_Server.Model
{
    public class Judgements : System.ComponentModel.INotifyPropertyChanged
    {
        private double markA;
        private double markB;
        private double markC;
        private double markD;
        private double markE;

        private double mark;

        private int area;

        // Updateing Silent only fires 1 event instead of 5 ...
        public void UpdateSilent(int area, double mark, double A, double B, double C, double D, double E)
        {
            this.mark = mark;
            this.area = area;

            markA = A;
            markB = B;
            markC = C;
            markD = D;
            markE = E;

            RaiseEvent("MarkA");
            RaiseEvent("MarkB");
            RaiseEvent("MarkC");
            RaiseEvent("MarkD");
            RaiseEvent("MarkE");
            // Total should also calculate the totals ...
            // via CalcualteTotals() Method
            RaiseEvent("Total");
        }

        public double Mark
        {
            get
            {
                return mark;
            }

            set
            {
                this.mark = value;
                RaiseEvent("Mark");
                RaiseEvent("Total");
            }
        }

        public double MarkA
        {
            get { return markA; }
            set
            {
                double old = markA;
                markA = value;
                if (markA == old) return;
                RaiseEvent("MarkA");
                RaiseEvent("Total");
            }
        }

        public double MarkB
        {
            get { return markB; }
            set
            {
                double old = markB;
                markB = value;
                if (markB == old) return;
                RaiseEvent("MarkB");
                RaiseEvent("Total");
            }
        }

        public double MarkC
        {
            get { return markC; }
            set
            {
                var old = markC;
                markC = value;
                if (markC == old) return;
                RaiseEvent("MarkC");
                RaiseEvent("Total");
            }
        }

        public double MarkD
        {
            get { return markD; }
            set
            {
                var old = markD;
                markD = value;
                if (markD == old) return;
                RaiseEvent("MarkD");
                RaiseEvent("Total");
            }
        }

        public double MarkE
        {
            get { return markE; }
            set
            {
                var old = markE;
                markE = value;
                if (markE == old) return;
                RaiseEvent("MarkE");
                RaiseEvent("Total");
            }
        }

        public string Area
        {
            get
            {
                switch (area)
                {
                    case 1:
                        return "TQ";
                    case 2:
                        return "MM";
                    case 3:
                        return "PS";
                    case 4:
                        return "CP";
                    case 5:
                        return "FS";
                    case 6:
                        return "PY";
                    case 7:
                        return "BL";
                }

                return "";
            }
        }

        public double Total
        {
            get
            {
                return markA + markB + markC + markD + markE;
            }
        }

        public string Dance { get; set; }
        public string Judge { get; set; }
        public int Couple { get; set; }

        public override string ToString()
        {
            return MarkA + ", " + MarkB + ", " + MarkC + ", " + MarkD + ", " + MarkE;
        }

        #region INotifyPropertyChanged Members

        protected void RaiseEvent(string Property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }

}
