using System;

namespace Judging_Server.Model
{
    public class EventData
    {
        public string Title { get; set; }
        public string Place { get; set; }
        public string Date { get; set; }
        public string Organizer { get; set; }
        public string WDSFId { get; set; }
        public string ScrutinusId { get; set; }
        public string RoundNr { get; set; }
        public DateTime Created { get; set; }
        public string Folder { get; set; }
        public string Version { get; set; }
    }
}