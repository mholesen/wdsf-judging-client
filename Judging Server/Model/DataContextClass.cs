using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;

using CommonLibrary;

using Judging_Server.Helper;
using Judging_Server.ReportModels;

namespace Judging_Server.Model
{
    public class DataContextClass : System.ComponentModel.INotifyPropertyChanged
    {

        private Dictionary<string, string> danceNameMap; 

        public DataContextClass()
        {
            this.AllJudgements = new List<Judgements>();

            danceNameMap = new Dictionary<string, string>()
                               {
                                   {"W", "Waltz"},
                                   {"SW", "Waltz"},
                                   {"TG", "Tango"},
                                   {"VW", "Viennese Waltz"},
                                   {"SF", "Slow Foxtrot"},
                                   {"QS", "Quickstep"},
                                   {"SB", "Samba"},
                                   {"CC", "Cha Cha Cha"},
                                   {"RB", "Rumba"},
                                   {"PD", "Paso Doble"},
                                   {"JV", "Jive"},
                                   {"SS", "Showdance Standard"},
                                   {"SL", "Showdance Latin"},
                                   {"FL", "Formation Latin"},
                                   {"FS", "Formation Standard"}
                               };
        }

        public static bool RaiseEvents { get; set; }

        // List of Dances and Directory of Judges
        /// <summary>
        /// This structur holds all marks of all judges, dances and couples
        /// </summary>
        public Dictionary<string, Dictionary<string, Dictionary<int, Judgements>>> judgements;
        /// <summary>
        /// Place Offset in B-Final to display TV results
        /// </summary>
        public int PlaceOffset { get; set; }
        /// <summary>
        /// Folder name for results (Final, Final A, Round 1)
        /// </summary>
        public string WDSGFolder { get; set; }
        /// <summary>
        /// If true, this competition is calculated with the new V2 Version.
        /// If false, its an old version 1 competition
        /// </summary>
        public CalculationVersionEnum? CalculationVersion { get; set; }
        /// <summary>
        /// True if competition is formation (different drawing for judges)
        /// </summary>
        public bool IsFormation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is final.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is final; otherwise, <c>false</c>.
        /// </value>
        public bool IsFinal { get; set; }
        /// <summary>
        /// List of Judging Areas
        /// </summary>
        public List<JudgingArea> JudgingAreas { get; set; }
        /// <summary>
        /// List of all Judgements
        /// </summary>
        public List<Judgements> AllJudgements { get; set; } 
        /// <summary>
        /// Dance Results by Dance (Dictionary holds per Dance the result of all Couples)
        /// </summary>
        private Dictionary<string, List<DanceResultBase>> danceResults;
        public Dictionary<string, List<DanceResultBase>> DanceResults
        {
            get
            {
                return this.danceResults;
            }
            set
            {
                this.danceResults = value;
            }
        }

        public double MinimumPoints { get; set; }

        public double MaximumPoints { get; set; }

        public double IntervallPoints { get; set; }

        /// <summary>
        /// Dance Result over all dances.
        /// </summary>
        public List<DanceResultBase> TotalResult { get; set; }
        /// <summary>
        /// States of all devices that are active
        /// </summary>
        public List<DeviceStates> DeviceState { get; set; }
        /// <summary>
        ///  List of all Judges in this Competition
        /// </summary>
        public List<Judge> Judges { get; set; }

        /// <summary>
        /// List of all Dances in this compettion
        /// </summary>
        private List<Dance> dances;

        public List<Dance> Dances
        {
            get
            {
                return this.dances;
            }
            set
            {
                this.dances = value;
            }
        }
        /// <summary>
        /// List of all Couples
        /// </summary>
        public List<Couple> Couples { get; set; }

        private Dictionary<string, List<Heat>> _heats;

        /// <summary>
        /// The Heats 
        /// </summary>
        public Dictionary<string, List<Heat>> Heats
        {
            get { return this._heats; }
            set
            {
                this._heats = value;
                // CreateSequenceLists();
            }
        }

        /// <summary>
        /// A List of all heats in the order they will be danced
        /// </summary>
        public ObservableCollection<Heat> HeatsSequence { get; set; }

        /// <summary>
        /// Drawing Judge - Area by Heats
        /// </summary>
        public Dictionary<string, List<JudgesDrawing>> JudgeDrawing { get; set; } 

        public List<DanceResultBase> AllResults { get; set; }

        public EventData Event { get; set; }

        public void CreateSequenceLists()
        {
            if (this.Heats.Count == 0) return;
            // Wir erzeugen nun noch eine Liste, die alle Daten in der Reihenfolge der
            // T�nze unter der Rundenauslosung darstellt. Das brauchen wir sp�ter
            // f�r eine einfache Beamersteuerung
            this.AllResults = new List<DanceResultBase>();
            this.HeatsSequence = new ObservableCollection<Heat>();

            foreach (var key in this.Heats.Keys)
            {
                var heats = this.Heats[key];
                var list = this.DanceResults[key];
                foreach (var heat in heats)
                {
                    this.HeatsSequence.Add(heat);
                    foreach (var couple in heat.Couples)
                    {
                        var dr = list.Single(d => d.Couple.Id == couple.Id);
                        this.AllResults.Add(dr);
                    }
                }                
            }

            this.RaiseEvent("HeatsSequence");
        }

        public void CreateDataModel()
        {
            RaiseEvents = true;

            this.judgements = new Dictionary<string, Dictionary<string, Dictionary<int, Judgements>>>();

            // Fill up the class with some data
            for (var d = 0; d < this.Dances.Count; d++)
            {
                var judgesDic = new Dictionary<string, Dictionary<int, Judgements>>();
                this.judgements.Add(this.Dances[d].ShortName, judgesDic);
                for (var j = 0; j < this.Judges.Count; j++)
                {
                    var dicCouples = new Dictionary<int, Judgements>();
                    judgesDic.Add(this.Judges[j].Sign, dicCouples);
                    for (int c = 0; c < this.Couples.Count; c++)
                    { 
                        var judgement = new Judgements() { Dance = this.Dances[d].ShortName, Judge = this.Judges[j].Sign, Couple = this.Couples[c].Id };
                        dicCouples.Add(this.Couples[c].Id, judgement);
                        judgement.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(this.Judgement_PropertyChanged);
                    }
                }
            }
            // We now create the data sets for the totals 
            this.DanceResults = new Dictionary<string, List<DanceResultBase>>();
            for (var d = 0; d < this.Dances.Count; d++)
            {
                var list = new List<DanceResultBase>();
                this.DanceResults.Add(this.Dances[d].ShortName, list);
                for(var c=0; c < this.Couples.Count;c++)
                {

                    var dr = DanceResultFactory.GetDanceResult(this.CalculationVersion.Value, this.JudgingAreas, this.Couples[c], this.Dances[d].ShortName, this.Judges);
                    dr.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(this.DanceResult_PropertyChanged);
                    list.Add(dr);
                }
            }
            // Total Dance Result ...
            this.TotalResult = new List<DanceResultBase>();
            for (var c = 0; c < this.Couples.Count; c++)
            {
                var dr = DanceResultFactory.GetDanceResult(this.CalculationVersion.Value, this.JudgingAreas, this.Couples[c], "Total", this.Judges);
                // for 10 dance, we might have an offset from the other section
                dr.Total = this.Couples[c].PointOffset;
                this.TotalResult.Add(dr);
            }

            // Und noch die �brigen Listen erzeugen:
            this.CreateSequenceLists();

        }

        void SortAndSetPlace(List<DanceResultBase> list)
        {

            list.Sort(new DanceResultSorter());
            // Nun vergeben wir die Pl�tze absteigend (Aufpassen, bei gleichen Summen �ndern sich die Pl�tze nicht
            int place = 1;
            int same = 0;
            for (int i = 0; i < list.Count; i++)
            {
                list[i].Place = place;
                if (i < list.Count - 1 && list[i + 1].Total != list[i].Total)
                {
                    place += 1 + same;
                    same = 0;
                }
                else
                {
                    same++;
                }
            }

            // Nun m�ssen wir einen Event feuern, dass sich der Platz ge�ndert hat.
        }

        void DanceResult_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Place" || e.PropertyName == "Total")
                return;

            var dr = (DanceResultBase)sender;
            // Wir m�ssen f�r den betroffenden Tanz m�glicherweise nun die Pl�tze neu berechnen ...
            var list = this.DanceResults[dr.Dance];
            // Wir sortieren nun die Liste Absteigend und setzen dann die Pl�tze richtig ein
            this.SortAndSetPlace(list);
            // nun haben wir f�r diesen Tanz den Platz gesetzt, nun machen wir das f�r alle T�nze
            double sum = 0;
            double sumA = 0;
            double sumB = 0;
            double sumC = 0;
            double SumD = 0;

            foreach (var key in this.DanceResults.Keys)
            {
                list = this.DanceResults[key];
                var danceResultList = list.Single(c => c.Couple == dr.Couple);
                
                sum += danceResultList.Total;
                sumA += danceResultList.Result_A;
                sumB += danceResultList.Result_B;
                sumC += danceResultList.Result_C;
                SumD += danceResultList.Result_D;
            }
            var totalResultOfCouple = this.TotalResult.Single(t => t.Couple == dr.Couple);

            totalResultOfCouple.Total = sum + dr.Couple.PointOffset;

            totalResultOfCouple.Result_A = sumA;
            totalResultOfCouple.Result_B = sumB;
            totalResultOfCouple.Result_C = sumC;
            totalResultOfCouple.Result_D = SumD;

            this.SortAndSetPlace(this.TotalResult);
        }

        void Judgement_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Total")
                return;

            var j = (Judgements)sender;
            // Wir brauchen nun den Total Result Wert hierf�r, dann passen wir das an ...
            var list = this.DanceResults[j.Dance];
            // Wir brauchen das richtige Paar aus unserer Liste
            var dr = list.Single(s => s.Couple.Id == j.Couple);
            dr.UpdateByJudge(j.Judge, j);
        }

        public void UpdateChairmanReduction(string dance, int couple, double reduction)
        {
            var drlist = this.DanceResults[dance];
            var dr = drlist.Single(s => s.Couple.Id == couple);
            dr.ChairmanReduction = reduction;
        }

        public void UpdateJudgement(string judge, string dance, int couple, int area, double mark, double A, double B, double C, double D, double E)
        {
            Judgements j;

            lock (this.judgements)
            {
                j = this.judgements[dance][judge][couple];
            }

            j.UpdateSilent(area, mark, A, B, C, D, E);

            // todo: herausfinden, ob das hier ben�tigt wird:
            j.Dance = dance;
            j.Judge = judge;
            j.Couple = couple;

            var j2 = this.AllJudgements.SingleOrDefault(o => o.Dance == dance && o.Judge == judge && o.Couple == couple);
            if (j2 == null)
            {
                this.AllJudgements.Add(j);
            }
        }

        public void LoadJSConfiguration(string file)
        {
            var doc = XDocument.Load(file);
            var root = doc.Element("JSConfiguration");

            var version = root.Element("JSVersion");
            if (version == null)
            {
                throw new Exception("No JSVersion set in JSConfiguration.xml");
            }

            this.WDSGFolder = ServerSettings.Default.WDSGExport + "\\" + version.Attribute("Section").Value;
            
            this.CalculationVersion = null;

            if (version.Attribute("Version").Value == "V2" || version.Attribute("Version").Value == "V3")
                this.CalculationVersion = Model.CalculationVersionEnum.Version2;
            if (version.Attribute("Version").Value == "V1")
                this.CalculationVersion = Model.CalculationVersionEnum.Version1;
            if (version.Attribute("Version").Value == "V2014")
                this.CalculationVersion = Model.CalculationVersionEnum.Version2014;

            this.IsFormation = version.Attribute("IsFormation").Value.ToLower() == "true";

            // Min, Max-Points
            var judgingData = root.Element("JudgingData");

            var dStr = judgingData.Attribute("MinScore").Value;
            this.MinimumPoints = dStr.ParseString();
            dStr = judgingData.Attribute("MaxScore").Value;
            this.MaximumPoints = dStr.ParseString();
            dStr = judgingData.Attribute("Intervall").Value;
            this.IntervallPoints = dStr.ParseString();

            // Now we load the components_

            // Judging Areas:
            this.JudgingAreas = new List<JudgingArea>();
            foreach (var areaXml in doc.Descendants("Component"))
            {
                var component = new JudgingArea()
                                    {
                                        Component = areaXml.Attribute("ShortName").Value,
                                        Description = areaXml.Value,
                                        Weight = areaXml.Attribute("Weight").Value.ParseString()
                                    };
                this.JudgingAreas.Add(component);
            }

        }

        public void LoadDataXML(string file)
        {
            var doc = XDocument.Load(file);
   
            // Read Event Data
            var root = doc.Element("Data");
            var eventData = root.Element("Event");
            if (eventData == null) throw new Exception("No event data in XML data file");

            this.IsFinal = eventData.Element("IsFinal").Value.ToLower() == "true";

            this.Event = new EventData()
                        {
                            RoundNr = eventData.Element("RoundNumber").Value,
                            ScrutinusId = eventData.Attribute("Id").Value,
                            Title = eventData.Element("Title").Value,
                            WDSFId = eventData.Attribute("WdsfId").Value
                        };

            
            var offset = 0;
            Int32.TryParse(eventData.Element("PlaceOffset").Value, out offset);
            this.PlaceOffset = offset;

            // Check the input folder exists
            var targetPath = ServerSettings.Default.DataPath + String.Format("\\{0}_{1}", this.Event.ScrutinusId, this.Event.RoundNr);
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            this.Dances = new List<Dance>();
            this.Judges = new List<Judge>();
            this.Couples = new List<Couple>();
           
            // Dances:
            foreach(var danceXml in doc.Descendants("Dance"))
            {
                var dance = new Dance()
                                {
                                    ShortName = danceXml.Attribute("ShortName").Value,
                                };
                // Fix dance names if needed:
                if (!danceNameMap.ContainsKey(dance.ShortName))
                {
                    throw new Exception("Could not find dance with short name " + dance.ShortName);
                }

                dance.LongName = danceNameMap[dance.ShortName];
                this.Dances.Add(dance);
            }
           
            // Now read the the judges:
            foreach(var judgeXml in doc.Descendants("Judge"))
            {
                var judge = new Judge()
                                {
                                    Country = judgeXml.Attribute("Country").Value,
                                    Name = judgeXml.Attribute("Name").Value,
                                    Sign = judgeXml.Attribute("Sign").Value,
                                    WDSFSign = judgeXml.Attribute("WdsfSign").Value,
                                    MIN = judgeXml.Attribute("MIN").Value, 
                                    ComponentToJudge = judgeXml.Attribute("Component") != null ? judgeXml.Attribute("Component").Value : null
                                };
                this.Judges.Add(judge);
            }
            // Read the couples
            foreach(var coupleXml in doc.Descendants("Couple"))
            {
                var couple = new Couple(){
                                             Id = Int32.Parse(coupleXml.Attribute("Number").Value),
                                             Name = coupleXml.Attribute("Name").Value,
                                             Country = coupleXml.Attribute("Country").Value,
                                             WDSF_ID = coupleXml.Attribute("MIN").Value
                                         };
                this.Couples.Add(couple);
            }

            this.Heats = new Dictionary<string, List<Heat>>();
            this.HeatsSequence = new ObservableCollection<Heat>();

            var heatIndex = 1;

            foreach (var heatXml in doc.Descendants("Heat"))
            {
                List<Heat> heats = null;
                var dance = heatXml.Attribute("Dance").Value;
                var couples = heatXml.Attribute("Couples").Value.Split(',');
                if (this.Heats.ContainsKey(dance))
                {
                    heats = this.Heats[dance];
                }
                else
                {
                    heats = new List<Heat>();
                    this.Heats.Add(dance, heats);
                }

                var heat = new Heat() { Id = heatIndex, Dance = this.Dances.Single(d => d.ShortName == dance) };
                heats.Add(heat);
                this.HeatsSequence.Add(heat);
                heatIndex++;

                heat.Couples = new List<Couple>();
                foreach (var coupleNo in couples)
                {
                    var id = Int32.Parse(coupleNo);
                    heat.Couples.Add(this.Couples.Single(c => c.Id == id));
                }

            }
        }

        private void LoadConfigurationXml(string fileName)
        {
            var doc = XDocument.Load(fileName);

            // Read Event Data
            var root = doc.Element("Data");
        }

        public void LoadData(string file)
        {
            this.Event = new EventData();

            var sr = new System.IO.StreamReader(file);

            this.Dances = new List<Dance>();
            this.Judges = new List<Judge>();
            this.Couples = new List<Couple>();

            var version = sr.ReadLine();
            {
                this.CalculationVersion = null;

                if(version == "V1")
                    this.CalculationVersion = CalculationVersionEnum.Version1;
                if (version == "V2")
                    this.CalculationVersion = CalculationVersionEnum.Version2;
                if(version == "V2014")
                    this.CalculationVersion = CalculationVersionEnum.Version2014;
                if (version.StartsWith("V3"))
                {
                    this.CalculationVersion = CalculationVersionEnum.Version2;
                    this.IsFormation = true;
                }
                else
                {
                    this.IsFormation = false;
                }
            }

            if (this.CalculationVersion == null)
                throw new Exception("Wrong Version string in data file");

            var strings = sr.ReadLine().Split(';');
            
            this.WDSGFolder =  ServerSettings.Default.WDSGExport;
            this.PlaceOffset = Int32.Parse(strings[1]);

            var numJudgingAreas = Int32.Parse(sr.ReadLine());
            this.JudgingAreas = new List<JudgingArea>();

            for (var i1 = 0; i1 < numJudgingAreas; i1++)
            {
                var data = sr.ReadLine().Split(';');
                this.JudgingAreas.Add(new JudgingArea
                                     {
                                         Component = data[0],
                                         Description = data[2],
                                         Weight = data[1].ParseString()
                                     });
                
            }

            var competitionData = sr.ReadLine().Split(';');
            
            this.Event.WDSFId = competitionData[0];
            this.Event.ScrutinusId = competitionData[1];
            this.Event.RoundNr = competitionData[2];
            this.Event.Title = competitionData[3];
            // Check the input folder exists
            var targetPath = ServerSettings.Default.DataPath +
                             String.Format("\\{0}_{1}", this.Event.ScrutinusId, this.Event.RoundNr);
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            string[] dancedata = sr.ReadLine().Split(';');
            int i = 0;
            for (i = 0; i < dancedata.Length - 1; i += 2)
            {
                var d = new Dance() { ShortName = dancedata[i], LongName = dancedata[i + 1] };
                this.Dances.Add(d);
            }
            // Load the Judges
            string line = sr.ReadLine();
            while (line.StartsWith("Judge"))
            {
                string[] judgeData = line.Split(';');
                line = sr.ReadLine();
                Judge j = new Judge() { Sign = judgeData[1], WDSFSign = judgeData[2], Name = judgeData[3], Country = judgeData[4] };
                this.Judges.Add(j);
            }
            // Load the Couples
            line = sr.ReadLine();
            while (line != ";")
            {   
                string[] coupleData = line.Split(';');
                Couple c = new Couple() { Id = Int32.Parse(coupleData[0]), Name = coupleData[1], Country = coupleData[2] };
                this.Couples.Add(c);
                line = sr.ReadLine();
            }

            sr.Close();
        }

        public void LoadJudgesDrawing(string file)
        {
            if (!File.Exists(file))
                return;

            var sr = new StreamReader(file);

            this.JudgeDrawing = new Dictionary<string, List<JudgesDrawing>>();

            while (!sr.EndOfStream)
            {
                var data = sr.ReadLine().Split(';');
                var dance = this.Dances.Single(d => d.ShortName == data[0]);
                var list = new List<JudgesDrawing>();
                this.JudgeDrawing.Add(dance.ShortName, list);

                var numHeats = Int32.Parse(data[1]);
                // Loop all heats
                var index = 2;
                for (var i = 0; i < numHeats; i++)
                {
                    var heatId = Int32.Parse(data[index]);
                    var heat = this.Heats[dance.ShortName].Single(h => h.Id == (heatId));
                    var area = Int32.Parse(data[index + 1]);
                    var numJudges = Int32.Parse(data[index + 2]);
                    var judgesList = new List<Judge>();

                    var jd = new JudgesDrawing() {Area = area, Heat = heat, Judges = judgesList};
                    list.Add(jd);
                    index += 3;
                    for (var j = 0; j < numJudges; j++)
                    {
                        var judge = this.Judges.Single(ju => ju.Sign == data[index]);
                        judgesList.Add(judge);
                        index++;
                    }
                }
            }
        }

        public void LoadHeats(string file)
        {
            
            var sr = new System.IO.StreamReader(file);

            this.Heats = new Dictionary<string, List<Heat>>();
            var counter = 1;
            while (!sr.EndOfStream)
            {
                var data = sr.ReadLine().Split(';');
                //SW;<numHeats;<size>;couples...;size;coupes...
                var list = new List<Heat>();
                this.Heats.Add(data[0], list);

                var dance = this.Dances.Single(d => d.ShortName == data[0]);

                var noHeats = Int32.Parse(data[1]);
                var index = 2;
                
                for (var i = 0; i < noHeats; i++)
                {
                    var heat = new Heat() {Couples = new List<Couple>(), Dance = dance, Id = counter};
                    counter++;

                    list.Add(heat);
                    var size = Int32.Parse(data[index]);
                    index++;
                    for (var j = 0; j < size; j++)
                    {
                        var id = Int32.Parse(data[index]);
                        var couple = this.Couples.SingleOrDefault(c => c.Id == id);
                        if (couple == null)
                        {
                            throw new Exception("A couple was in the list of heats but not qualified. Heats and Qualified do not match, please delete heats");
                        }

                        heat.Couples.Add(couple);
                        index++;
                    }
                }
            }
        }

        public void LoadDevices(string filename)
        {
            this.DeviceState = new List<DeviceStates>();
            var sr = new System.IO.StreamReader(filename);
            while (!sr.EndOfStream)
            {
                var line = sr.ReadLine();
                if(!line.StartsWith(";"))
                {
                    var data = line.Split(';');
                    var ds = new DeviceStates
                                 {
                                     DeviceId = data[0],
                                     DevicePath = data[1],
                                     Position = data[2],
                                     State = "",
                                     Heat = 0,
                                     Dance = 0,
                                     Area = 0d
                                 };
                    this.DeviceState.Add(ds);
                }
            }
            sr.Close();
            // Assign Judges to Devices
            // This might be done again later
            for (int i = 0; i < this.Judges.Count; i++)
            {
                this.DeviceState[i].Judge = this.Judges[i].Sign;
            }
        }


        private char CharFromIndex(int index)
        {
            int a = (int) 'A' + index;
            
            return (char) a;
        }

        private string getColumname(int index)
        {
            int last = index / 26;
            int first = index - (last*26);

            if (index > 25)
            {
                last -= 1;
                return "" + this.CharFromIndex(last) + this.CharFromIndex(first);
            }
            else
            {
                return "" + this.CharFromIndex(first);
            }
        }

        public void ExportExcelV2(System.IO.StreamWriter writer)
        {
            // We export the data per judge compared with the coleques from the same component
            foreach(var judge in this.Judges)
            {
                foreach(var couple in this.Couples)
                {
                    var avg = 0d;
                    var judgeTotal = 0d;
                    foreach(var dance in this.Dances)
                    {
                        // Get judgements:
                        var judgements = this.AllJudgements.Where(j => j.Couple == couple.Id && j.Dance == dance.ShortName);
                        // what component did our judge judge ...
                        var judgesJudgement = judgements.SingleOrDefault(j => j.Judge == judge.Sign);
                        if (judgesJudgement == null)
                        {
                            MessageBox.Show("Not all markings available, cannot export");
                            return;
                        }
                        if(judgesJudgement.MarkA > 0)
                        {
                            avg += judgements.Where(j => j.MarkA > 0 && j.Judge != judge.Sign).Sum(j => j.MarkA) / ((double)judgements.Count(j => j.MarkA > 0 && j.Judge != judge.Sign));
                            judgeTotal += judgesJudgement.MarkA;
                        }
                        if (judgesJudgement.MarkB > 0)
                        {
                            avg += judgements.Where(j => j.MarkB > 0 && j.Judge != judge.Sign).Sum(j => j.MarkB) / ((double)judgements.Count(j => j.MarkB > 0 && j.Judge != judge.Sign));
                            judgeTotal += judgesJudgement.MarkB;
                        }
                        if (judgesJudgement.MarkC > 0)
                        {
                            avg += judgements.Where(j => j.MarkC > 0 && j.Judge != judge.Sign).Sum(j => j.MarkC) / ((double)judgements.Count(j => j.MarkC > 0 && j.Judge != judge.Sign));
                            judgeTotal += judgesJudgement.MarkC;
                        }
                        if (judgesJudgement.MarkD > 0)
                        {
                            avg += judgements.Where(j => j.MarkD > 0 && j.Judge != judge.Sign).Sum(j => j.MarkD) / ((double)judgements.Count(j => j.MarkD > 0 && j.Judge != judge.Sign));
                            judgeTotal += judgesJudgement.MarkD;
                        }
                    }
                    writer.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7}", couple.Id, couple.Name, couple.Country, judge.Sign, judge.Name, judge.Country, avg, judgeTotal);
                }
            }
        }

        public void ExportExcel(System.IO.StreamWriter writer)
        {
            writer.WriteLine(this.Event.Title + ", " + this.Event.Place);
            // wir schreiben nun erst einmal die Liste der Judges
            writer.Write(";;");
            foreach (var judge in this.Judges)
            {
                writer.Write(judge.Sign + " :" + judge.Name + ";;;;;;;");
            }
            writer.WriteLine("");
            var danceCount = 0;
            var line = 3;
            foreach (var dance in this.Dances)
            {
                var allres = this.AllResults.Where(d => d.Dance == dance.ShortName).OrderBy(c => c.Couple).ToList();
                // Jetzt pro Paar:
                foreach (var couple in this.Couples)
                {
                    writer.Write(dance.LongName + ";" + couple.Name + ";");
                    int col = 2;
                    
                    foreach (var judge in this.Judges)
                    {
                        Judgements j = this.judgements[dance.ShortName][judge.Sign][couple.Id];

                        writer.Write(String.Format("{0};{1};{2};{3};{4};=SUMME({5}{6}:{7}{6});;", j.MarkA, j.MarkB,j.MarkC,j.MarkD, j.MarkE, this.getColumname(col), line, this.getColumname(col + 4) ));
                        col += 7;
                    }
                    // Nun noch die Mittelwertformel
                    writer.WriteLine("");
                    line++;
                }
                int column = 1;
                writer.Write(";;");
                for (int i = 0; i < this.Judges.Count; i++)
                {
                    for (int x = 0; x < 5; x++)
                    {
                        int startzeile = 3 + (danceCount*(this.Couples.Count + 2));

                        column++;
                        writer.Write(String.Format("=MITTELWERT({0}{2}:{1}{3});", this.getColumname(column), this.getColumname(column), startzeile, startzeile + this.Couples.Count - 1));
                    }
                    writer.Write(";;");
                    column += 2;
                }

                danceCount++;
                writer.WriteLine("");
                writer.WriteLine("");
                line += 2;
            }
        }

        #region INotifyPropertyChanged Members

        protected void RaiseEvent(string Property)
        {
            if (this.PropertyChanged != null && DataContextClass.RaiseEvents)
                this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void SaveEvent()
        {
            var sw = new System.IO.StreamWriter(ServerSettings.Default.DataPath + "\\event.txt");

            sw.WriteLine(DateTime.Now.Year);
            sw.WriteLine(this.Event.Date ?? DateTime.Now.ToShortDateString());
            sw.Write(this.Event.Place ?? "");
            sw.WriteLine(this.Event.Organizer ?? "");
            sw.WriteLine(this.Event.WDSFId ?? "unknown ID");

            sw.Close();
        }

        private void SavaDataFile()
        {
            var sw = new System.IO.StreamWriter(ServerSettings.Default.DataPath + "\\data.txt");
            // write dances
            var line = "";
            foreach (var dance in this.Dances)
            {
                line += line == ""
                            ? String.Format("{0};{1}", dance.ShortName, dance.LongName)
                            : String.Format(";{0};{1}", dance.ShortName, dance.LongName);
            }
            sw.WriteLine(line);
            // write Judges
            foreach (var judge in this.Judges)
            {
                sw.WriteLine(String.Format("Judge;{0};{1};{2};{3};{4}", judge.Sign, judge.WDSFSign, judge.Name, judge.Country, judge.MIN));
            }
            // One empty line as marker -> now write couples
            sw.WriteLine(";");
            foreach (var couple in this.Couples)
            {
                sw.WriteLine(String.Format("{0};{1};{2};{3}", couple.Id, couple.Name, couple.Country, couple.WDSF_ID));
            }
            sw.WriteLine(";");
            sw.Close();
        }

        public void SaveData()
        {
            // Wir erzeugen unsere Datendateien f�r Judges etc
            // Write Event:
            this.SaveEvent();
            this.SavaDataFile();
        }

        public IEnumerable<BestWorstAnalysisPrintModel> CreateBestWorstPrintModel()
        {
            var result = new List<BestWorstAnalysisPrintModel>();

            if (this.AllJudgements == null)
                return result;

            foreach (var couple in this.Couples)
            {
                foreach (var dance in this.Dances)
                {
                    // We do it per Dance

                    // Component 
                    if (!this.AllJudgements.Any(j => j.Couple == couple.Id && j.Dance == dance.ShortName & j.MarkA > 0))
                    {
                        continue;
                    }

                    var pm = new BestWorstAnalysisPrintModel()
                                 {
                                     Name = couple.NiceName,
                                     Number = couple.Id,
                                     Dance = dance.LongName
                                 };

                    var criticalLevel = 0.99;

                    result.Add(pm);

                    var marks =
                        this.AllJudgements.Where(j => j.Couple == couple.Id && j.Dance == dance.ShortName & j.MarkA > 0).OrderBy(m => m.MarkA).ToList();
                    if (marks.Count() != 3)
                        continue;

                    // Maybe we could do it nicer with reflection?
                    pm.BestWorstAreaA = new JudgeAreaViewModel()
                                            {
                                                Best =
                                                    new JudgeBestWorsPrintModel()
                                                        {
                                                            DistanceMedia = marks[2].MarkA - marks[1].MarkA,
                                                            JudgeSign = marks[2].Judge,
                                                            IsToFar = marks[2].MarkA - marks[1].MarkA > criticalLevel ? true : false
                                                        },

                                                Worst = new JudgeBestWorsPrintModel()
                                                            {
                                                                DistanceMedia = marks[1].MarkA - marks[0].MarkA,
                                                                JudgeSign = marks[0].Judge,
                                                                IsToFar = marks[1].MarkA - marks[0].MarkA > criticalLevel ? true : false
                                                            },

                                                Media = new JudgeBestWorsPrintModel()
                                                            {
                                                                DistanceMedia = marks[1].MarkA,
                                                                JudgeSign = marks[1].Judge,
                                                                IsToFar = false
                                                            },
                                            };

                    marks =
                        this.AllJudgements.Where(j => j.Couple == couple.Id && j.Dance == dance.ShortName & j.MarkB > 0).OrderBy(m => m.MarkB).ToList();

                    pm.BestWorstAreaB = new JudgeAreaViewModel()
                                            {
                                                Best =
                                                    new JudgeBestWorsPrintModel()
                                                        {
                                                            DistanceMedia = marks[2].MarkB - marks[1].MarkB,
                                                            JudgeSign = marks[2].Judge,
                                                            IsToFar = marks[2].MarkB - marks[1].MarkB > criticalLevel ? true : false
                                                        },

                                                Worst = new JudgeBestWorsPrintModel()
                                                            {
                                                                DistanceMedia = marks[1].MarkB - marks[0].MarkB,
                                                                JudgeSign = marks[0].Judge,
                                                                IsToFar = marks[1].MarkB - marks[0].MarkB > criticalLevel ? true : false
                                                            },

                                                Media = new JudgeBestWorsPrintModel()
                                                            {
                                                                DistanceMedia = marks[1].MarkB,
                                                                JudgeSign = marks[1].Judge,
                                                                IsToFar = false
                                                            },
                                            };

                    marks =
                        this.AllJudgements.Where(j => j.Couple == couple.Id && j.Dance == dance.ShortName & j.MarkC > 0).OrderBy(m => m.MarkC).ToList();

                    pm.BestWorstAreaC = new JudgeAreaViewModel()
                                            {
                                                Best =
                                                    new JudgeBestWorsPrintModel()
                                                        {
                                                            DistanceMedia = marks[2].MarkC - marks[1].MarkC,
                                                            JudgeSign = marks[2].Judge,
                                                            IsToFar = marks[2].MarkC - marks[1].MarkC > criticalLevel ? true : false
                                                        },

                                                Worst = new JudgeBestWorsPrintModel()
                                                            {
                                                                DistanceMedia = marks[1].MarkC - marks[0].MarkC,
                                                                JudgeSign = marks[0].Judge,
                                                                IsToFar = marks[1].MarkC - marks[0].MarkC > criticalLevel ? true : false
                                                            },

                                                Media = new JudgeBestWorsPrintModel()
                                                            {
                                                                DistanceMedia = marks[1].MarkC,
                                                                JudgeSign = marks[1].Judge,
                                                                IsToFar = false
                                                            },
                                            };

                    marks =
                        this.AllJudgements.Where(j => j.Couple == couple.Id && j.Dance == dance.ShortName & j.MarkD > 0).OrderBy(m => m.MarkD).ToList();

                    pm.BestWorstAreaD = new JudgeAreaViewModel()
                                            {
                                                Best =
                                                    new JudgeBestWorsPrintModel()
                                                        {
                                                            DistanceMedia = marks[2].MarkD - marks[1].MarkD,
                                                            JudgeSign = marks[2].Judge,
                                                            IsToFar = marks[2].MarkD - marks[1].MarkD > criticalLevel ? true : false
                                                        },

                                                Worst = new JudgeBestWorsPrintModel()
                                                            {
                                                                DistanceMedia = marks[1].MarkD - marks[0].MarkD,
                                                                JudgeSign = marks[0].Judge,
                                                                IsToFar = marks[1].MarkD - marks[0].MarkD > criticalLevel ? true : false
                                                            },

                                                Media = new JudgeBestWorsPrintModel()
                                                            {
                                                                DistanceMedia = marks[1].MarkD,
                                                                JudgeSign = marks[1].Judge,
                                                                IsToFar = false
                                                            },
                                            };
                }
            }

            return result;
        }

        public void WriteResultExchange(string filename)
        {
            

            var stream = new StreamWriter(filename);

            foreach (var danceResult in this.TotalResult)
            {
                stream.WriteLine();
            }
        }
    }
}