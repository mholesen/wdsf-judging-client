using System.Collections.Generic;

namespace Judging_Server.Model
{
    public class DanceResultSorter : IComparer<DanceResultBase>
    {

        #region IComparer<DanceResult> Members

        public int Compare(DanceResultBase x, DanceResultBase y)
        {
            return y.Total.CompareTo(x.Total);
        }

        #endregion
    }
}