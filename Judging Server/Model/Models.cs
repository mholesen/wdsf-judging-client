﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Media;
using System.Windows;
using Judging_Server.Helper;
using System.Xml.Linq;
using Judging_Server.ReportModels;

namespace Judging_Server.Model
{

#region EventHandlerArgs
    public class ResultAvailableEventArgs
    {
        public string Dance { get; set; }
        public int Couple { get; set; }
    }
#endregion

    #region Device State
    
    public class DeviceStates : System.ComponentModel.INotifyPropertyChanged
    {
        /// <summary>
        /// Position of Device, used for Formation EventsS
        /// </summary>
        public string Position { get; set; }

        public string DeviceId { get; set; }

        private int _dance;

        public int Dance
        {
            get { return _dance; }
            set { _dance = value;
            RaiseEvent("Dance");    
            }
        }

        private int _heat;
        public int Heat
        {
            get { return _heat; }
            set { _heat = value;
            RaiseEvent("Heat");
            }
        }
        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value;
            RaiseEvent("State");
            }
        }

        private DateTime _time;
        public DateTime Time
        {
            get { return _time; }
            set { _time = value; RaiseEvent("Time"); }
        }

        private double _mark;
        public double Area
        {
            get { return _mark; }
            set { _mark = value; RaiseEvent("Area"); }
        }

        public string DevicePath { get; set; }

        // Sign of assigned Judge
        public string Judge { get; set; }

        #region INotifyPropertyChanged Members

        protected void RaiseEvent(string Property)
        {
            if (PropertyChanged != null && DataContextClass.RaiseEvents)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }

    #endregion

    public class EventData
    {
        public string Title { get; set; }
        public string Place { get; set; }
        public string Date { get; set; }
        public string Organizer { get; set; }
        public string WDSFId { get; set; }
        public string ScrutinusId { get; set; }
        public string RoundNr { get; set; }
        public DateTime Created { get; set; }
        public string Folder { get; set; }
        public string Version { get; set; }
    }

    public class TotalPerCouple : System.ComponentModel.INotifyPropertyChanged
    {
        private double _total;
        private int _state;

        public double Total {
            get { return _total; }
            set
            {
                _total = value; RaiseEvent("Total");
            }
        }
        public int State { 
            get { return _state; }
            set
            {
                _state = value; 
                RaiseEvent("State");
            } 
        }
        public string Judge { get; set; }
        public int Couple {get; set; }    

        #region INotifyPropertyChanged Members

        protected void RaiseEvent(string Property)
        {
            if (PropertyChanged != null && DataContextClass.RaiseEvents)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
    
    /// <summary>
    /// Compares the total sums to sort the list by result ...
    /// </summary>
    public class TotalPerCoupleSorter : IComparer<TotalPerCouple>
    {
    
    #region IComparer<TotalPerCouple> Members

        public int  Compare(TotalPerCouple x, TotalPerCouple y)
        {
 	       return x.Total.CompareTo(y.Total);
        }

    #endregion
    }

    public enum States
    {
        Used = 1,
        NotUsedMinimum = 2,
        NotUsedMaximum = 3,
        NotUsed = 4
    }

    public class StateToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
     object parameter, CultureInfo culture)
        {
            var s = (States)value;
            switch (s)
            {
                case States.NotUsed: return Visibility.Hidden;
                default: return Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class StateToBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            var s = (States)value;
            switch(s)
            {
                case States.Used: return new SolidColorBrush(Colors.White);
                case States.NotUsedMaximum: return new SolidColorBrush(Colors.Red);
                case States.NotUsedMinimum: return new SolidColorBrush(Colors.Yellow);
                case States.NotUsed: return new SolidColorBrush(Colors.Black);
                default: return new SolidColorBrush(Colors.White);
            }
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class DictionaryToValueConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var sIndex = parameter as string;

            if (sIndex == null) return null;

            var index = 0;
            if (!Int32.TryParse(sIndex, out index)) return null;



            var dict = (Dictionary<string, TotalPerCouple>) value;
            if (dict == null) return null;
            if (index >= dict.Keys.Count) return null;

            var key = dict.Keys.ToList()[index];

            return dict[key].Total;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

 
    public class DanceResultSorter : IComparer<DanceResultBase>
    {

        #region IComparer<DanceResult> Members

        public int Compare(DanceResultBase x, DanceResultBase y)
        {
            return y.Total.CompareTo(x.Total);
        }

        #endregion
    }

    public class Dance
    {
        public string ShortName { get; set; }
        public string LongName { get; set; }
    }

    public class Judge
    {
        public string Sign { get; set; }
        public string WDSFSign { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string MIN { get; set; }
    }

    public class Couple
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string WDSF_ID { get; set; }
        public string NiceName
        {
            get { return String.Format("({0}) - {1}", Id, Name); }
        }
    }

    public class JudgingArea
    {
        public string Component { get; set; }
        public string Description { get; set; }
        public double Weight {get; set;}
    }
    
    public class DataContextClass : System.ComponentModel.INotifyPropertyChanged
    {

        public static bool RaiseEvents { get; set; }

        // List of Dances and Directory of Judges
        /// <summary>
        /// This structur holds all marks of all judges, dances and couples
        /// </summary>
        public Dictionary<string, Dictionary<string, Dictionary<int, Judgements>>> judgements;
        /// <summary>
        /// Place Offset in B-Final to display TV results
        /// </summary>
        public int PlaceOffset { get; set; }
        /// <summary>
        /// Folder name for results (Final, Final A, Round 1)
        /// </summary>
        public string WDSGFolder { get; set; }
        /// <summary>
        /// If true, this competition is calculated with the new V2 Version.
        /// If false, its an old version 1 competition
        /// </summary>
        public CalculationVersionEnum? CalculationVersion { get; set; }
        /// <summary>
        /// True if competition is formation (different drawing for judges)
        /// </summary>
        public bool IsFormation { get; set; }
        /// <summary>
        /// List of Judging Areas
        /// </summary>
        public List<JudgingArea> JudgingAreas { get; set; }
        /// <summary>
        /// List of all Judgements
        /// </summary>
        public List<Judgements> AllJudgements { get; set; } 
        /// <summary>
        /// Dance Results by Dance (Dictionary holds per Dance the result of all Couples)
        /// </summary>
        public Dictionary<string, List<DanceResultBase>> DanceResults{ get; set; }
        /// <summary>
        /// Dance Result over all dances.
        /// </summary>
        public List<DanceResultBase> TotalResult { get; set; }
        /// <summary>
        /// States of all devices that are active
        /// </summary>
        public List<DeviceStates> DeviceState { get; set; }
        /// <summary>
        ///  List of all Judges in this Competition
        /// </summary>
        public List<Judge> Judges { get; set; }
        /// <summary>
        /// List of all Dances in this compettion
        /// </summary>
        public List<Dance> Dances { get; set; }
        /// <summary>
        /// List of all Couples
        /// </summary>
        public List<Couple> Couples { get; set; }

        private Dictionary<string, List<Heat>> _heats;

        /// <summary>
        /// The Heats 
        /// </summary>
        public Dictionary<string, List<Heat>> Heats
        {
            get { return _heats; }
            set
            {
                _heats = value;
                // CreateSequenceLists();
            }
        }

        /// <summary>
        /// A List of all heats in the order they will be danced
        /// </summary>
        public ObservableCollection<Heat> HeatsSequence { get; set; }

        /// <summary>
        /// Drawing Judge - Area by Heats
        /// </summary>
        public Dictionary<string, List<JudgesDrawing>> JudgeDrawing { get; set; } 

        public List<DanceResultBase> AllResults { get; set; }

        public EventData Event { get; set; }

        public void CreateSequenceLists()
        {
            if (Heats.Count == 0) return;
            // Wir erzeugen nun noch eine Liste, die alle Daten in der Reihenfolge der
            // Tänze unter der Rundenauslosung darstellt. Das brauchen wir später
            // für eine einfache Beamersteuerung
            AllResults = new List<DanceResultBase>();
            HeatsSequence = new ObservableCollection<Heat>();

            foreach (var key in Heats.Keys)
            {
                var heats = Heats[key];
                var list = DanceResults[key];
                foreach (var heat in heats)
                {
                    HeatsSequence.Add(heat);
                    foreach (var couple in heat.Couples)
                    {
                        var dr = list.Single(d => d.Couple == couple.Id);
                        AllResults.Add(dr);
                    }
                }                
            }

            RaiseEvent("HeatsSequence");
        }

        public void CreateDataModel()
        {
            RaiseEvents = true;

            judgements = new Dictionary<string, Dictionary<string, Dictionary<int, Judgements>>>();

            // Fill up the class with some data
            for (var d = 0; d < Dances.Count; d++)
            {
                var judgesDic = new Dictionary<string, Dictionary<int, Judgements>>();
                judgements.Add(Dances[d].ShortName, judgesDic);
                for (var j = 0; j < Judges.Count; j++)
                {
                    var dicCouples = new Dictionary<int, Judgements>();
                    judgesDic.Add(Judges[j].Sign, dicCouples);
                    for (int c = 0; c < Couples.Count; c++)
                    { 
                        var judgement = new Judgements() { Dance = Dances[d].ShortName, Judge = Judges[j].Sign, Couple = Couples[c].Id };
                        dicCouples.Add(Couples[c].Id, judgement);
                        judgement.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(Judgement_PropertyChanged);
                    }
                }
            }
            // We now create the data sets for the totals 
            DanceResults = new Dictionary<string, List<DanceResultBase>>();
            for (var d = 0; d < Dances.Count; d++)
            {
                var list = new List<DanceResultBase>();
                DanceResults.Add(Dances[d].ShortName, list);
                for(var c=0; c < Couples.Count;c++)
                {

                    var dr = DanceResultFactory.GetDanceResult(CalculationVersion.Value, JudgingAreas, Couples[c].Id, Dances[d].ShortName, Judges);
                    dr.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(DanceResult_PropertyChanged);
                    list.Add(dr);
                }
            }
            // Total Dance Result ...
            TotalResult = new List<DanceResultBase>();
            for (var c = 0; c < Couples.Count; c++)
            {
                var dr = DanceResultFactory.GetDanceResult(CalculationVersion.Value, JudgingAreas, Couples[c].Id, "Total", Judges);
                TotalResult.Add(dr);
            }

            // Und noch die übrigen Listen erzeugen:
            CreateSequenceLists();

        }

        void SortAndSetPlace(List<DanceResultBase> list)
        {

            list.Sort(new DanceResultSorter());
            // Nun vergeben wir die Plätze absteigend (Aufpassen, bei gleichen Summen ändern sich die Plätze nicht
            int place = 1;
            int same = 0;
            for (int i = 0; i < list.Count; i++)
            {
                list[i].Place = place;
                if (i < list.Count - 1 && list[i + 1].Total != list[i].Total)
                {
                    place += 1 + same;
                    same = 0;
                }
                else
                {
                    same++;
                }
            }

            // Nun müssen wir einen Event feuern, dass sich der Platz geändert hat.
        }

        void DanceResult_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Place" || e.PropertyName == "Total")
                return;

            var dr = (DanceResultBase)sender;
            // Wir müssen für den betroffenden Tanz möglicherweise nun die Plätze neu berechnen ...
            var list = DanceResults[dr.Dance];
            // Wir sortieren nun die Liste Absteigend und setzen dann die Plätze richtig ein
            SortAndSetPlace(list);
            // nun haben wir für diesen Tanz den Platz gesetzt, nun machen wir das für alle Tänze
            double sum = 0;
            double sumA = 0;
            double sumB = 0;
            double sumC = 0;
            double SumD = 0;

            foreach (var key in DanceResults.Keys)
            {
                list = DanceResults[key];
                var danceResultList = list.Single(c => c.Couple == dr.Couple);
                
                sum += danceResultList.Total;
                sumA += danceResultList.Result_A;
                sumB += danceResultList.Result_B;
                sumC += danceResultList.Result_C;
                SumD += danceResultList.Result_D;
            }
            var totalResultOfCouple = TotalResult.Single(t => t.Couple == dr.Couple);

            totalResultOfCouple.Total = sum;
            totalResultOfCouple.Result_A = sumA;
            totalResultOfCouple.Result_B = sumB;
            totalResultOfCouple.Result_C = sumC;
            totalResultOfCouple.Result_D = SumD;

            SortAndSetPlace(TotalResult);
        }

        void Judgement_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Total")
                return;

            var j = (Judgements)sender;
            // Wir brauchen nun den Total Result Wert hierfür, dann passen wir das an ...
            var list = DanceResults[j.Dance];
            // Wir brauchen das richtige Paar aus unserer Liste
            var dr = list.Single(s => s.Couple == j.Couple);
            dr.UpdateByJudge(j.Judge, j);
        }

        public void UpdateChairmanReduction(string dance, int couple, double reduction)
        {
            var drlist = DanceResults[dance];
            var dr = drlist.Single(s => s.Couple == couple);
            dr.ChairmanReduction = reduction;
        }

        public void UpdateJudgement(string judge, string dance, int couple, double A, double B, double C, double D, double E)
        {
            Judgements j = null;

            lock (judgements)
            {
                try
                {
                    j = judgements[dance][judge][couple];
                }
                catch (KeyNotFoundException ex)
                {
                    if (!judgements.ContainsKey(dance))
                    {
                        throw new KeyNotFoundException("Could not find Dance " + dance + " while updating markings. Marking and dance data does not match");
                    }

                    if (!judgements[dance].ContainsKey(judge))
                    {
                        throw new KeyNotFoundException("Could not find Judge " + judge + " while updating markings. Marking and dance data does not match");
                    }

                    if (!judgements[dance][judge].ContainsKey(couple))
                    {
                        throw new KeyNotFoundException("Could not find couple " + couple + " while updating markings. Marking and dance data does not match");
                    }
                }
            }

            j.UpdateSilent(A, B, C, D, E);

            // todo: herausfinden, ob das hier benötigt wird:
            j.Dance = dance;
            j.Judge = judge;
            j.Couple = couple;
            
            if(AllJudgements == null)
                AllJudgements = new List<Judgements>();

            var j2 = AllJudgements.SingleOrDefault(o => o.Dance == dance && o.Judge == judge && o.Couple == couple);
            if (j2 == null)
                AllJudgements.Add(j);
        }

        public void LoadDataXML(string file)
        {
            var doc = XDocument.Load(file);
   
            // Read Event Data
            var root = doc.Element("Data");
            var eventData = root.Element("Event");
            if (eventData == null) throw new Exception("No event data in XML data file");

            CalculationVersion = null;

            if (eventData.Attribute("Version").Value == "V2" || eventData.Attribute("Version").Value == "V3")
                CalculationVersion = Model.CalculationVersionEnum.Version2;
            if (eventData.Attribute("Version").Value == "V1")
                CalculationVersion = Model.CalculationVersionEnum.Version1;
            if(eventData.Attribute("Version").Value == "V2014")
                CalculationVersion = Model.CalculationVersionEnum.Version2014;

            if(CalculationVersion == null)
                throw new Exception("Calculation Version is not set!");

            if (eventData.Attribute("IsFormation").ToString().ToLower() == "true")
                IsFormation = true;
            else
                IsFormation = false;

            WDSGFolder = eventData.Element("WDSGFolder").Value;

            var offset = 0;
            Int32.TryParse(eventData.Element("PlaceOffset").Value, out offset);
            PlaceOffset = offset;

            Event = new EventData()
            {
                RoundNr = eventData.Element("RoundNumber").Value,
                ScrutinusId = eventData.Attribute("Id").Value,
                Title = eventData.Element("Title").Value,
                WDSFId = eventData.Attribute("WdsfId").Value
            };

            // Check the input folder exists
            var targetPath = ServerSettings.Default.DataPath +
                             String.Format("\\{0}_{1}", Event.ScrutinusId, Event.RoundNr);
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            Dances = new List<Dance>();
            Judges = new List<Judge>();
            Couples = new List<Couple>();
            JudgingAreas = new List<JudgingArea>();
            // Dances:
            foreach(var danceXml in doc.Descendants("Dance"))
            {
                var dance = new Dance()
                {
                    ShortName = danceXml.Attribute("ShortName").Value,
                    LongName = danceXml.Attribute("LongName").Value
                };
                Dances.Add(dance);
            }
            var decimalChar = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            // Judging Areas:
            foreach(var areaXml in doc.Descendants("Component"))
            {
                var component = new JudgingArea()
                {
                    Component = areaXml.Attribute("ShortName").Value,
                    Description = areaXml.Value,
                    Weight = double.Parse(areaXml.Attribute("Weight").Value.Replace(".", decimalChar).Replace(",", decimalChar))
                };
                JudgingAreas.Add(component);
            }
            // Now read the the judges:
            foreach(var judgeXml in doc.Descendants("Judge"))
            {
                var judge = new Judge()
                {
                    Country = judgeXml.Attribute("Country").Value,
                    Name = judgeXml.Attribute("Name").Value,
                    Sign = judgeXml.Attribute("Sign").Value,
                    WDSFSign = judgeXml.Attribute("WdsfSign").Value,
                    MIN = judgeXml.Attribute("MIN").Value 
                };
                Judges.Add(judge);
            }
            // Finally, read the judges
            foreach(var coupleXml in doc.Descendants("Couple"))
            {
                var couple = new Couple(){
                    Id = Int32.Parse(coupleXml.Attribute("Number").Value),
                    Name = coupleXml.Attribute("Name").Value,
                    Country = coupleXml.Attribute("Country").Value,
                    WDSF_ID = coupleXml.Attribute("MIN").Value
                };
                Couples.Add(couple);
            }
        }

        public void LoadData(string file)
        {
            Event = new EventData();

            var sr = new System.IO.StreamReader(file);

            Dances = new List<Dance>();
            Judges = new List<Judge>();
            Couples = new List<Couple>();

            var version = sr.ReadLine();
            {
                CalculationVersion = null;

                if(version == "V1")
                    CalculationVersion = CalculationVersionEnum.Version1;
                if (version == "V2")
                    CalculationVersion = CalculationVersionEnum.Version2;
                if(version == "V2014")
                    CalculationVersion = CalculationVersionEnum.Version2014;
                if (version.StartsWith("V3"))
                {
                    CalculationVersion = CalculationVersionEnum.Version2;
                    IsFormation = true;
                }
                else
                {
                    IsFormation = false;
                }
            }

            if (CalculationVersion == null)
                throw new Exception("Wrong Version string in data file");

            var strings = sr.ReadLine().Split(';');
            WDSGFolder = strings[0];
            PlaceOffset = Int32.Parse(strings[1]);

            var numJudgingAreas = Int32.Parse(sr.ReadLine());
            JudgingAreas = new List<JudgingArea>();

            for (var i1 = 0; i1 < numJudgingAreas; i1++)
            {
                var data = sr.ReadLine().Split(';');
                JudgingAreas.Add(new JudgingArea
                {
                    Component = data[0],
                    Description = data[2],
                    Weight = DoubleHelper.ParseString(data[1])
                });
                
            }

            var competitionData = sr.ReadLine().Split(';');
            
            Event.WDSFId = competitionData[0];
            Event.ScrutinusId = competitionData[1];
            Event.RoundNr = competitionData[2];
            Event.Title = competitionData[3];
            // Check the input folder exists
            var targetPath = ServerSettings.Default.DataPath +
                             String.Format("\\{0}_{1}", Event.ScrutinusId, Event.RoundNr);
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            string[] dancedata = sr.ReadLine().Split(';');
            int i = 0;
            for (i = 0; i < dancedata.Length - 1; i += 2)
            {
                var d = new Dance() { ShortName = dancedata[i], LongName = dancedata[i + 1] };
                Dances.Add(d);
            }
            // Load the Judges
            string line = sr.ReadLine();
            while (line.StartsWith("Judge"))
            {
                string[] judgeData = line.Split(';');
                line = sr.ReadLine();
                Judge j = new Judge() { Sign = judgeData[1], WDSFSign = judgeData[2], Name = judgeData[3], Country = judgeData[4] };
                Judges.Add(j);
            }
            // Load the Couples
            line = sr.ReadLine();
            while (line != ";")
            {   
                string[] coupleData = line.Split(';');
                Couple c = new Couple() { Id = Int32.Parse(coupleData[0]), Name = coupleData[1], Country = coupleData[2] };
                Couples.Add(c);
                line = sr.ReadLine();
            }

            sr.Close();
        }

        public void LoadJudgesDrawing(string file)
        {
            if (!System.IO.File.Exists(file))
                return;

            var sr = new System.IO.StreamReader(file);

            JudgeDrawing = new Dictionary<string, List<JudgesDrawing>>();

            while (!sr.EndOfStream)
            {
                var data = sr.ReadLine().Split(';');
                var dance = Dances.Single(d => d.ShortName == data[0]);
                var list = new List<JudgesDrawing>();
                JudgeDrawing.Add(dance.ShortName, list);

                var numHeats = Int32.Parse(data[1]);
                // Loop all heats
                var index = 2;
                for (var i = 0; i < numHeats; i++)
                {
                    var heatId = Int32.Parse(data[index]);
                    var heat = Heats[dance.ShortName].Single(h => h.Id == heatId);
                    var area = Int32.Parse(data[index + 1]);
                    var numJudges = Int32.Parse(data[index + 2]);
                    var judgesList = new List<Judge>();

                    var jd = new JudgesDrawing() {Area = area, Heat = heat, Judges = judgesList};
                    list.Add(jd);
                    index += 3;
                    for (var j = 0; j < numJudges; j++)
                    {
                        var judge = Judges.Single(ju => ju.Sign == data[index]);
                        judgesList.Add(judge);
                        index++;
                    }
                }
            }
        }

        public void LoadHeats(string file)
        {
            
            var sr = new System.IO.StreamReader(file);

            Heats = new Dictionary<string, List<Heat>>();
            var counter = 0;
            while (!sr.EndOfStream)
            {
                var data = sr.ReadLine().Split(';');
                //SW;<numHeats;<size>;couples...;size;coupes...
                var list = new List<Heat>();
                Heats.Add(data[0], list);

                var dance = Dances.Single(d => d.ShortName == data[0]);

                var noHeats = Int32.Parse(data[1]);
                var index = 2;
                
                for (var i = 0; i < noHeats; i++)
                {
                    var heat = new Heat() {Couples = new List<Couple>(), Dance = dance, Id = counter};
                    counter++;

                    list.Add(heat);
                    var size = Int32.Parse(data[index]);
                    index++;
                    for (var j = 0; j < size; j++)
                    {
                        var id = Int32.Parse(data[index]);
                        var couple = Couples.SingleOrDefault(c => c.Id == id);
                        if (couple == null)
                        {
                            throw new Exception("A couple was in the list of heats but not qualified. Heats and Qualified do not match, please delete heats");
                        }

                        heat.Couples.Add(couple);
                        index++;
                    }
                }
            }

            // Now, as we have heats, create the data model
            CreateDataModel();
        }

        public void LoadDevices(string filename)
        {
            DeviceState = new List<DeviceStates>();
            var sr = new System.IO.StreamReader(filename);
            while (!sr.EndOfStream)
            {
                var line = sr.ReadLine();
                if(!line.StartsWith(";"))
                {
                    var data = line.Split(';');
                    var ds = new DeviceStates
                        {
                            DeviceId = data[0],
                            DevicePath = data[1],
                            Position = data[2],
                            State = "",
                            Heat = 0,
                            Dance = 0,
                            Area = 0d
                        };
                    DeviceState.Add(ds);
                }
            }
            sr.Close();
            // Assign Judges to Devices
            // This might be done again later
            for (int i = 0; i < Judges.Count; i++)
            {
                DeviceState[i].Judge = Judges[i].Sign;
            }
        }


        private char CharFromIndex(int index)
        {
            int a = (int) 'A' + index;
            
            return (char) a;
        }

        private string getColumname(int index)
        {
            int last = index / 26;
            int first = index - (last*26);

            if (index > 25)
            {
                last -= 1;
                return "" + CharFromIndex(last) + CharFromIndex(first);
            }
            else
            {
                return "" + CharFromIndex(first);
            }
        }

        public void ExportExcelV2(System.IO.StreamWriter writer)
        {
            // We export the data per judge compared with the coleques from the same component
            foreach(var judge in Judges)
            {
                foreach(var couple in Couples)
                {
                    var avg = 0d;
                    var judgeTotal = 0d;
                    foreach(var dance in Dances)
                    {
                        // Get judgements:
                        var judgements = AllJudgements.Where(j => j.Couple == couple.Id && j.Dance == dance.ShortName);
                        // what component did our judge judge ...
                        var judgesJudgement = judgements.Single(j => j.Judge == judge.Sign);
                        if(judgesJudgement.MarkA > 0)
                        {
                            avg += judgements.Where(j => j.MarkA > 0 && j.Judge != judge.Sign).Sum(j => j.MarkA) / ((double)judgements.Count(j => j.MarkA > 0 && j.Judge != judge.Sign));
                            judgeTotal += judgesJudgement.MarkA;
                        }
                        if (judgesJudgement.MarkB > 0)
                        {
                            avg += judgements.Where(j => j.MarkB > 0 && j.Judge != judge.Sign).Sum(j => j.MarkB) / ((double)judgements.Count(j => j.MarkB > 0 && j.Judge != judge.Sign));
                            judgeTotal += judgesJudgement.MarkB;
                        }
                        if (judgesJudgement.MarkC > 0)
                        {
                            avg += judgements.Where(j => j.MarkC > 0 && j.Judge != judge.Sign).Sum(j => j.MarkC) / ((double)judgements.Count(j => j.MarkC > 0 && j.Judge != judge.Sign));
                            judgeTotal += judgesJudgement.MarkC;
                        }
                        if (judgesJudgement.MarkD > 0)
                        {
                            avg += judgements.Where(j => j.MarkD > 0 && j.Judge != judge.Sign).Sum(j => j.MarkD) / ((double)judgements.Count(j => j.MarkD > 0 && j.Judge != judge.Sign));
                            judgeTotal += judgesJudgement.MarkD;
                        }
                    }
                    writer.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7}", couple.Id, couple.Name, couple.Country, judge.Sign, judge.Name, judge.Country, avg, judgeTotal);
                }
            }
        }

        public void ExportExcel(System.IO.StreamWriter writer)
        {
            writer.WriteLine(Event.Title + ", " + Event.Place);
            // wir schreiben nun erst einmal die Liste der Judges
            writer.Write(";;");
            foreach (var judge in Judges)
            {
                writer.Write(judge.Sign + " :" + judge.Name + ";;;;;;;");
            }
            writer.WriteLine("");
            var danceCount = 0;
            var line = 3;
            foreach (var dance in Dances)
            {
                var allres = AllResults.Where(d => d.Dance == dance.ShortName).OrderBy(c => c.Couple).ToList();
                // Jetzt pro Paar:
                foreach (var couple in Couples)
                {
                    writer.Write(dance.LongName + ";" + couple.Name + ";");
                    int col = 2;
                    
                    foreach (var judge in Judges)
                    {
                        Judgements j = judgements[dance.ShortName][judge.Sign][couple.Id];

                        writer.Write(String.Format("{0};{1};{2};{3};{4};=SUMME({5}{6}:{7}{6});;", j.MarkA, j.MarkB,j.MarkC,j.MarkD, j.MarkE, getColumname(col), line, getColumname(col + 4) ));
                        col += 7;
                    }
                    // Nun noch die Mittelwertformel
                    writer.WriteLine("");
                    line++;
                }
                int column = 1;
                writer.Write(";;");
                for (int i = 0; i < Judges.Count; i++)
                {
                    for (int x = 0; x < 5; x++)
                    {
                        int startzeile = 3 + (danceCount*(Couples.Count + 2));

                        column++;
                        writer.Write(String.Format("=MITTELWERT({0}{2}:{1}{3});", getColumname(column), getColumname(column), startzeile, startzeile + Couples.Count - 1));
                    }
                    writer.Write(";;");
                    column += 2;
                }

                danceCount++;
                writer.WriteLine("");
                writer.WriteLine("");
                line += 2;
            }
        }

        #region INotifyPropertyChanged Members

        protected void RaiseEvent(string Property)
        {
            if (PropertyChanged != null && DataContextClass.RaiseEvents)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void SaveEvent()
        {
            var sw = new System.IO.StreamWriter(ServerSettings.Default.DataPath + "\\event.txt");

            sw.WriteLine(DateTime.Now.Year);
            sw.WriteLine(Event.Date ?? DateTime.Now.ToShortDateString());
            sw.Write(Event.Place ?? "");
            sw.WriteLine(Event.Organizer ?? "");
            sw.WriteLine(Event.WDSFId ?? "unknown ID");

            sw.Close();
        }

        private void SavaDataFile()
        {
            var sw = new System.IO.StreamWriter(ServerSettings.Default.DataPath + "\\data.txt");
            // write dances
            var line = "";
            foreach (var dance in Dances)
            {
                line += line == ""
                            ? String.Format("{0};{1}", dance.ShortName, dance.LongName)
                            : String.Format(";{0};{1}", dance.ShortName, dance.LongName);
            }
            sw.WriteLine(line);
            // write Judges
            foreach (var judge in Judges)
            {
                sw.WriteLine(String.Format("Judge;{0};{1};{2};{3};{4}", judge.Sign, judge.WDSFSign, judge.Name, judge.Country, judge.MIN));
            }
            // One empty line as marker -> now write couples
            sw.WriteLine(";");
            foreach (var couple in Couples)
            {
                sw.WriteLine(String.Format("{0};{1};{2};{3}", couple.Id, couple.Name, couple.Country, couple.WDSF_ID));
            }
            sw.WriteLine(";");
            sw.Close();
        }

        public void SaveData()
        {
            // Wir erzeugen unsere Datendateien für Judges etc
            // Write Event:
            SaveEvent();
            SavaDataFile();
        }

        public IEnumerable<BestWorstAnalysisPrintModel> CreateBestWorstPrintModel()
        {
            var result = new List<BestWorstAnalysisPrintModel>();

            if (AllJudgements == null)
                return result;

            foreach (var couple in this.Couples)
            {
                foreach (var dance in Dances)
                {
                    // We do it per Dance

                    // Component 
                    if (!AllJudgements.Any(j => j.Couple == couple.Id && j.Dance == dance.ShortName & j.MarkA > 0))
                    {
                        continue;
                    }

                    var pm = new BestWorstAnalysisPrintModel()
                    {
                        Name = couple.NiceName,
                        Number = couple.Id,
                        Dance = dance.LongName
                    };

                    var criticalLevel = 0.99;

                    result.Add(pm);

                    var marks =
                        AllJudgements.Where(j => j.Couple == couple.Id && j.Dance == dance.ShortName & j.MarkA > 0).OrderBy(m => m.MarkA).ToList();
                    if (marks.Count() != 3)
                        continue;

                    // Maybe we could do it nicer with reflection?
                    pm.BestWorstAreaA = new JudgeAreaViewModel()
                    {
                        Best =
                            new JudgeBestWorsPrintModel()
                            {
                                DistanceMedia = marks[2].MarkA - marks[1].MarkA,
                                JudgeSign = marks[2].Judge,
                                IsToFar = marks[2].MarkA - marks[1].MarkA > criticalLevel ? true : false
                            },

                        Worst = new JudgeBestWorsPrintModel()
                        {
                            DistanceMedia = marks[1].MarkA - marks[0].MarkA,
                            JudgeSign = marks[0].Judge,
                            IsToFar = marks[1].MarkA - marks[0].MarkA > criticalLevel ? true : false
                        },

                        Media = new JudgeBestWorsPrintModel()
                        {
                            DistanceMedia = 0,
                            JudgeSign = marks[1].Judge,
                            IsToFar = false
                        },
                    };

                    marks =
                        AllJudgements.Where(j => j.Couple == couple.Id && j.Dance == dance.ShortName & j.MarkB > 0).OrderBy(m => m.MarkB).ToList();

                    pm.BestWorstAreaB = new JudgeAreaViewModel()
                    {
                        Best =
                            new JudgeBestWorsPrintModel()
                            {
                                DistanceMedia = marks[2].MarkB - marks[1].MarkB,
                                JudgeSign = marks[2].Judge,
                                IsToFar = marks[2].MarkB - marks[1].MarkB > criticalLevel ? true : false
                            },

                        Worst = new JudgeBestWorsPrintModel()
                        {
                            DistanceMedia = marks[1].MarkB - marks[0].MarkB,
                            JudgeSign = marks[0].Judge,
                            IsToFar = marks[1].MarkB - marks[0].MarkB > criticalLevel ? true : false
                        },

                        Media = new JudgeBestWorsPrintModel()
                        {
                            DistanceMedia = 0,
                            JudgeSign = marks[1].Judge,
                            IsToFar = false
                        },
                    };

                    marks =
                        AllJudgements.Where(j => j.Couple == couple.Id && j.Dance == dance.ShortName & j.MarkC > 0).OrderBy(m => m.MarkC).ToList();

                    pm.BestWorstAreaC = new JudgeAreaViewModel()
                    {
                        Best =
                            new JudgeBestWorsPrintModel()
                            {
                                DistanceMedia = marks[2].MarkC - marks[1].MarkC,
                                JudgeSign = marks[2].Judge,
                                IsToFar = marks[2].MarkC - marks[1].MarkC > criticalLevel ? true : false
                            },

                        Worst = new JudgeBestWorsPrintModel()
                        {
                            DistanceMedia = marks[1].MarkC - marks[0].MarkC,
                            JudgeSign = marks[0].Judge,
                            IsToFar = marks[1].MarkC - marks[0].MarkC > criticalLevel ? true : false
                        },

                        Media = new JudgeBestWorsPrintModel()
                        {
                            DistanceMedia = 0,
                            JudgeSign = marks[1].Judge,
                            IsToFar = false
                        },
                    };

                    marks =
                        AllJudgements.Where(j => j.Couple == couple.Id && j.Dance == dance.ShortName & j.MarkD > 0).OrderBy(m => m.MarkD).ToList();

                    pm.BestWorstAreaD = new JudgeAreaViewModel()
                    {
                        Best =
                            new JudgeBestWorsPrintModel()
                            {
                                DistanceMedia = marks[2].MarkD - marks[1].MarkD,
                                JudgeSign = marks[2].Judge,
                                IsToFar = marks[2].MarkD - marks[1].MarkD > criticalLevel ? true : false
                            },

                        Worst = new JudgeBestWorsPrintModel()
                        {
                            DistanceMedia = marks[1].MarkD - marks[0].MarkD,
                            JudgeSign = marks[0].Judge,
                            IsToFar = marks[1].MarkD - marks[0].MarkD > criticalLevel ? true : false
                        },

                        Media = new JudgeBestWorsPrintModel()
                        {
                            DistanceMedia = 0,
                            JudgeSign = marks[1].Judge,
                            IsToFar = false
                        },
                    };
                }
            }

            return result;
        }
    }

    public class StateToColorConverter : IValueConverter
    {

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int state = (int)value;

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
