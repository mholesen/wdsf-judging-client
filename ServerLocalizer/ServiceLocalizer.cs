﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ServerLocalizer
{
    internal class ServiceLocalizer
    {
        private UdpClient client = null;

        public string SendBroadcast(int port, string message)
        {
            if (client == null)
            {
                client = new UdpClient(9124);
            }

            byte[] packet = Encoding.ASCII.GetBytes(message);
            var timeToWait = TimeSpan.FromSeconds(5);

            try
            {
                client.Send(packet, packet.Length, IPAddress.Broadcast.ToString(), port);

                IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);

                
                var result = client.BeginReceive(null, null);
                result.AsyncWaitHandle.WaitOne(timeToWait);

                if (result.IsCompleted)
                {
                    client.EndReceive(result, ref sender);
                }
                else
                {
                    return null;
                }

                Console.WriteLine("Server Address is " + sender.Address.ToString());

                return sender.Address.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }
    }
}
